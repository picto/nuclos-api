//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api;

import java.io.Serializable;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

public class PreferencesImpl extends ConcurrentHashMap<String, Serializable>
	implements Preferences {
		
	private static final Logger LOG = Logger.getLogger(PreferencesImpl.class);
	
	private static final long serialVersionUID = -7094457981078544249L;
	
	@Override
	public void setPreference(String preference, Serializable value) {
		if (null != value) {
			put(preference, value);
		} else { 
			// implementation doesn't allow null keys
			remove(preference);
		}
	}
	
	@Override
	public void removePreference(String preference) {
		remove(preference);
	}
	
	@Override
	public Serializable getPreference(String preference) {
		return get(preference);
	}
	
	@Override
	public <S extends Serializable> S getPreference(String preference, Class<S> cls) {
		final Object value = get(preference);
		try {
			return cls.cast(value);
		}
		catch (ClassCastException e) {
			LOG.error("On " + this + " field " + preference + " value " + value + " expected type " + cls, e);
			throw e;
		}
	}
	
	@Override
	public Set<String> getPreferenceNames() {
		return keySet();
	}
}
