//Copyright (C) 2012  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.service;

import java.util.List;

import org.nuclos.api.businessobject.BusinessObject;
import org.nuclos.api.businessobject.Process;
import org.nuclos.api.businessobject.Query;
import org.nuclos.api.businessobject.facade.thin.Stateful;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.statemodel.State;

public interface QueryService {
	
	<PK, T extends BusinessObject<PK>> Query<T> createQuery(Class<T> type);
	
	<PK, T extends BusinessObject<PK>> List<T> executeQuery(Query<T> query);
	
	<PK, T extends BusinessObject<PK>> T executeQuerySingleResult(Query<T> query) throws BusinessException;
	
	<PK, T extends BusinessObject<PK>> T getById(Class<T> classtype, PK Id);
	
	<PK, T extends Stateful & BusinessObject<PK>> List<T> getByProcess(Process<T> process, Process<T>... additionalProcesses) throws BusinessException;
	
	<PK, T extends Stateful & BusinessObject<PK>> List<T> getByState(Class<T> type, State state, State... additionalStates) throws BusinessException; 
}

