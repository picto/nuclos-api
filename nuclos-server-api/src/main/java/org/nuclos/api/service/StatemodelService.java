//Copyright (C) 2012  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.service;

import org.nuclos.api.businessobject.facade.thin.Stateful;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.statemodel.State;


public interface StatemodelService {

	/**
	 * Method changes the state of the given BusinessObject if possible and valid
	 * 
	 * @param t
	 * @param iNumeral
	 * @throws Exception
	 * 
	 * @deprecated  - Please use method changeState(T t, State state) for type-safety
	 * 
	 */
	@Deprecated
	public <T extends Stateful> void changeState(T t, int iNumeral) throws Exception;
	
	/**
	 * Method changes the state of the BusinessObject t. The given state sState is validated
	 * and a BusinessException is thrown back if its not a valid transition in the statemodel
	 * the BusinessObject is attached to.
	 * 
	 * @param t - BusinessObject
	 * @param sState - State of statemodel that is attached to the entity/BusinessObject
	 * @throws BusinessException
	 * 
	 */
	public <T extends Stateful> void changeState(T t, State sState) throws BusinessException;
	
}
