//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.service;

import org.nuclos.api.communication.CommunicationPort;
import org.nuclos.api.communication.CommunicationPortKey;
import org.nuclos.api.context.communication.InstantiableContext;
import org.nuclos.api.context.communication.NotificationContext;
import org.nuclos.api.context.communication.RequestContext;
import org.nuclos.api.exception.BusinessException;

public interface CommunicationService {
	
	/**
	 * 
	 * @param context
	 * @throws BusinessException
	 */
	void handleNotification(NotificationContext context) throws BusinessException;
	
	/**
	 * 
	 * @param context
	 * @throws BusinessException
	 */
	void executeRequest(RequestContext<?> context) throws BusinessException;
	
	/**
	 * 
	 * @param context
	 * @throws BusinessException
	 */
	void handleResponse(RequestContext<?> context) throws BusinessException;
	
	/**
	 * For use in rules.
	 * @param portKey
	 * @param contextClass
	 * @return
	 * 		a new instance of the given context class 
	 * @throws BusinessException 
	 */
	<T extends InstantiableContext> T newContextInstance(CommunicationPortKey portKey, Class<T> contextClass) throws BusinessException;
	
	/**
	 * For internal use in ports.
	 * @param port
	 * @param contextClass
	 * @return
	 * 		a new instance of the given context class 
	 * @throws BusinessException 
	 */
	<T extends InstantiableContext> T newContextInstance(CommunicationPort port, Class<T> contextClass) throws BusinessException;

}
