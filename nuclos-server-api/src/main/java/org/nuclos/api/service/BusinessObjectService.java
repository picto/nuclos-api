//Copyright (C) 2012  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.service;

import java.util.Collection;

import org.nuclos.api.NuclosImage;
import org.nuclos.api.businessobject.BusinessObject;
import org.nuclos.api.businessobject.facade.LogicalDeletable;
import org.nuclos.api.common.NuclosFile;
import org.nuclos.api.exception.BusinessException;

public interface BusinessObjectService {

	public <T extends BusinessObject> Long insert(T type) throws BusinessException;
	public <T extends BusinessObject> Collection<Long> insertAll(Collection<T> type) throws BusinessException;
	
	public <T extends BusinessObject> void update(T type) throws BusinessException;
	public <T extends BusinessObject> void updateAll(Collection<T> type) throws BusinessException;
	
	public <T extends BusinessObject> void delete(T type) throws BusinessException;
	public <T extends BusinessObject> void deleteAll(Collection<T> type) throws BusinessException;
	
	<PK, T extends LogicalDeletable> void deleteLogical(T type) throws BusinessException;
	<PK, T extends LogicalDeletable> void deleteLogicalAll(Collection<T> type) throws BusinessException;
	
	public NuclosFile  newFile(String name, byte[] content) throws BusinessException;
	public NuclosImage newImage(String name, byte[] content) throws BusinessException;
	
}
