//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.service;

import org.nuclos.api.common.NuclosFile;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.objectimport.ImportResult;
import org.nuclos.api.objectimport.XmlImportStructureDefinition;

public interface XmlImportService {

	/**
	 * This method executes an XML import process by which the content of a {@link NuclosFile} is parsed using the given XML structure definitions
	 * and stored in the database afterwards. 
	 * The import can be handled as one atomic transaction with a complete rollback for one occurring error by setting argument 'isTransactional' to true
	 * @param importFile
	 * @param isTransactional
	 * @param structureDefClasses
	 * @return {@link ImportResult}
	 * @throws BusinessException
	 * 
	 * @author Thomas Pasch
	 */
	ImportResult run(NuclosFile importFile, boolean isTransactional, Class<? extends XmlImportStructureDefinition>... structureDefClasses)
			throws BusinessException;
	
}
