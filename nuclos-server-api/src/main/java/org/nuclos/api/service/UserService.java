package org.nuclos.api.service;

import java.util.Date;

import org.nuclos.api.common.NuclosRole;
import org.nuclos.api.common.NuclosUser;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.provider.BusinessObjectProvider;
import org.nuclos.api.provider.QueryProvider;

/**
 * The {@link UserService} provides several methods to create users and manipulate user role data
 * @author reichama
 *
 */
public interface UserService {

	/**
	 * This method creates a Nuclos User and returns the id of the new object. Password is created by Nuclos automatically.
	 * Afterwards a notification Email containing the password is sent to the user.<br> 
	 * <b>Note</b>: A new user does not have any roles and therefore does not have any rights in Nuclos. Use method addRole() and removeRole() for modifications
	 * 
	 * @param username, firstname, lastname, email, passwordChangeRequired
	 * @return UID
	 * @throws BusinessException
	 */
	public org.nuclos.api.UID insert(String username, String firstname, String lastname, String email, Boolean passwordChangeRequired) throws BusinessException;
	
	/**
	 * This method creates a Nuclos User and returns the id of the new object. A password must be provided.
	 * Afterwards a notification Email containing the password is sent to the user.<br> 
	 * <b>Note</b>: A new user does not have any roles and therefore does not have any rights in Nuclos. Use method addRole() and removeRole() for modifications
	 * 
	 * @param username, firstname, lastname, email, password, passwordChangeRequired
	 * @return UID
	 * @throws BusinessException
	 */
	public org.nuclos.api.UID insert(String username, String firstname, String lastname, String email, String password, Boolean passwordChangeRequired) throws BusinessException;
	
	/**
	 * This method assigns a role to a given user. The class of the user role is generated automatically and
	 * corresponds to an existing user role in Nuclos. The {@link NuclosUser} can be extracted by using {@link QueryProvider} or
	 * {@link BusinessObjectProvider} 
	 * 
	 * @param role
	 * @param user
	 * @return
	 * @throws BusinessException
	 */
	public org.nuclos.api.UID grantRole(Class<? extends NuclosRole> role, NuclosUser user) 
		throws BusinessException; 
	
	/**
	 * This method dispossesses a user of the given role. The class of the user role is generated automatically and
	 * corresponds to an existing user role in Nuclos. The {@link NuclosUser} can be extracted by using {@link QueryProvider} or
	 * {@link BusinessObjectProvider}. If the user is not assigned to the role a {@link BusinessException} is thrown.
	 * 
	 * @param role
	 * @param user
	 * @throws BusinessException
	 */
	public void revokeRole(Class<? extends NuclosRole> role, NuclosUser user) 
			throws BusinessException;

	/**
	 * This method allows to define a date on which to expire a specific user account. 
	 * The {@link NuclosUser} can be extracted by using {@link QueryProvider} or
	 * {@link BusinessObjectProvider}.
	 * 
	 * @param user
	 * @param date
	 * @throws BusinessException
	 */
	public void expire(NuclosUser user, Date date)
		throws BusinessException; 
	
}
