//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.service;

import java.io.File;

import org.nuclos.api.common.NuclosFile;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.print.PrintProperties;

public interface FileService {

	void print(NuclosFile file) throws BusinessException;
	void print(NuclosFile file, String printerName) throws BusinessException;
	void print(NuclosFile file, PrintProperties printProperties) throws BusinessException;
	NuclosFile toPdf(NuclosFile file) throws BusinessException;
	void save(NuclosFile file, String directory) throws BusinessException;
	NuclosFile newFile(File file) throws BusinessException;
	NuclosFile newFile(String completeFilePath) throws BusinessException;
}
