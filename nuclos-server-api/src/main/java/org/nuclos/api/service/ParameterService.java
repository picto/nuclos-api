package org.nuclos.api.service;

import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.parameter.NucletParameter;
import org.nuclos.api.parameter.SystemParameter;

public interface ParameterService {

	public String getSystemParameter(SystemParameter parameter)
			throws BusinessException;
	
	public String getNucletParameter(NucletParameter parameter)
			throws BusinessException;
		
}
