package org.nuclos.api.service;

import java.util.List;

import org.nuclos.api.locale.NuclosLocale;

public interface DataLocaleService {

	List<NuclosLocale> getDataLocales();

}
