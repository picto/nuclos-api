//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.service;

import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.ws.WebServiceObject;

/**
 * The {@link WebService} provides methods to create and initialize stub clients
 * for calling web-services.
 * 
 * @author reichama
 *
 */
public interface WebService {
	
	/**
	 * This method creates a new instance of the given stub class and provides it with all settings (e.g. Proxy, Authentication)
	 * that are deposited for it by the administration within Nuclos.<br>
	 * 
	 * {@link WebServiceObject} 
	 * 
	 * @param Class<T> stub class
	 * @param WebServiceObject webService
	 * @return T instancea of stub class
	 * @throws BusinessException
	 */
	public <T> T getStub(Class<T> stubClass,  WebServiceObject webService) throws BusinessException;
}
