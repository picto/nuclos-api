//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.communication;

import java.util.Map;

import org.nuclos.api.UID;
import org.nuclos.api.exception.BusinessException;

public interface CommunicationPortFactory<P extends CommunicationPort> {
	
	public interface InstanceContext {
		
		/**
		 * Nuclos generates Ids for all communication ports. Store the Id. 
		 * Maybe you want to search for Nuclos users who are assigned to it. 
		 * @return the Id of this port
		 */
		public UID getPortId();
		
		/**
		 * Your interface defines the needed system parameters. 
		 * Here you get the values configured in the administration.
		 * @return the parameter values
		 * 			Map of name and values, both Strings.
		 */
		public Map<String, String> getSystemParameterValues();
		
	}
	
	/**
	 * 
	 * @param context
	 * @return
	 * 		a new instance in order to communicate with the Nuclos server
	 * @throws BusinessException 
	 * 			Throw a BusinessException if your port is not ready.
	 * 			The exception message is displayed in the administration.
	 */
	public P newInstance(InstanceContext context) throws BusinessException;
	
	/**
	 * 
	 * @throws BusinessException
	 */
	public void shutdown(P port) throws BusinessException;	
	
}
