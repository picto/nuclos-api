package org.nuclos.api.provider;

import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.parameter.NucletParameter;
import org.nuclos.api.parameter.SystemParameter;
import org.nuclos.api.service.ParameterService;

public class ParameterProvider {

/**
 * This class is used in rule programming and provides methods for retrieving
 * system parameters 
 * </p>
 * @author Matthias Reichart
 */
private static ParameterService service;
	
	ParameterProvider() {
	}
	
	public void setParameterService(ParameterService parameterService) {
		this.service = parameterService;
	}
	
	private static ParameterService getService() {
		if (service == null) {
			throw new IllegalStateException("too early (missing in spring context?)");
		}
		return service;
	}
	
	/**
	 * This method returns the value of the given {@link SystemParameter} as String   
	 * 
	 * @param parameter {@link SystemParameter} - parameter to read
	 */
	public static String getSystemParameter(SystemParameter parameter) throws BusinessException {
		return getService().getSystemParameter(parameter);
	}
	
	/**
	 * This method returns the value of the given {@link NucletParameter} as String   
	 * 
	 * @param parameter {@link NucletParameter} - parameter to read
	 */
	public static String getNucletParameter(NucletParameter parameter)
			throws BusinessException {
		return getService().getNucletParameter(parameter);
	}
	
}
