//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.provider;

import org.nuclos.api.common.NuclosFile;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.objectimport.ImportResult;
import org.nuclos.api.objectimport.ImportStructureDefinition;
import org.nuclos.api.service.ImportService;

/**
 * {@link ImportProvider} provides methods to import data into Nuclos
 * @author reichama
 *
 */
public class ImportProvider {

	private static ImportService INSTANCE;
	
	ImportProvider() {
	}
	
	public void setImportService(ImportService repService) {
		INSTANCE = repService;
	}
	
	private static ImportService getService() {
		if (INSTANCE == null) {
			throw new IllegalStateException("too early (missing in spring context?)");
		}
		return INSTANCE;
	}
	
	/**
	 * This method executes an import process by which the content of a {@link NuclosFile} is parsed using the given structure definitions
	 * and stored in the database afterwards. 
	 * The import can be handled as one atomic transaction with a complete rollback for one occurring error by setting argument 'isTransactional' to true
	 * @param importFile
	 * @param isTransactional
	 * @param structureDefClasses
	 * @return
	 * @throws BusinessException
	 */
	public static ImportResult run(NuclosFile importFile, boolean isTransactional, Class<? extends ImportStructureDefinition>... structureDefClasses)
			throws BusinessException {
		return getService().run(importFile, isTransactional, structureDefClasses);
	}

}
