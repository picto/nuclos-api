package org.nuclos.api.provider;

import java.util.List;

import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.locale.NuclosLocale;
import org.nuclos.api.service.DataLocaleService;

public class DataLocaleProvider {

	private static DataLocaleService SERVICE;
	
	DataLocaleProvider() {}
	
	public void setDataLocaleService(DataLocaleService service) {
		this.SERVICE = service;
	}
	
	private static DataLocaleService getService() {
		if (SERVICE == null) {
			throw new IllegalStateException("too early (missing in spring context?)");
		}
		return SERVICE;
	}
	
	/**
	 * This method returns all active {@link NuclosLocale}. (installation depending)
	 * 
	 * @return List<NuclosLocale>
	 */
	public static List<NuclosLocale> getDataLocales() throws BusinessException {
		return getService().getDataLocales();
	}
	
}
