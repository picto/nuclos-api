//Copyright (C) 2012  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.provider;

import java.util.List;

import org.nuclos.api.businessobject.BusinessObject;
import org.nuclos.api.businessobject.Process;
import org.nuclos.api.businessobject.Query;
import org.nuclos.api.businessobject.facade.thin.Stateful;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.service.QueryService;
import org.nuclos.api.statemodel.State;

/**
 * {@link QueryProvider} provides methods for creating and executing
 * {@link Query} for data access and methods to read
 * single {@link BusinessObject}.
 * <p>
 * This Class is usually used in Rule-classes and allows API access only
 * </p>
 * @see Query
 * @see BusinessObject
 * @author Matthias Reichart
 */
public class QueryProvider {

	private static QueryService SERVICE;
	
	QueryProvider() {
	}
	
	public void setQueryService(QueryService service) {
		if (service == null) {
			throw new IllegalStateException("too early (missing in spring context?)");
		}
		this.SERVICE = service;
	}
	
	private static QueryService getService() {
		if (SERVICE == null) {
			throw new IllegalStateException("too early");
		}
		return SERVICE;
	}
	
	/**
	 * This methods returns an instance of {@link Query} that is type-specific to the class given
	 * as parameter. The {@link Query} can executed by using method <code>execute(BusinessObjectQuery&lt;T&gt; query)</code>
	 * 
	 * @param type - type-specific class that determines the type of the returned {@link Query}
	 * @return {@link Query}
	 * 
	 */
	public static <PK,T extends BusinessObject<PK>> Query<T> create(Class<T> type) {
		return getService().createQuery(type);
	}
	
	/**
	 * This methods executes a {@link Query} and returns a list of found {@link BusinessObject} of the type
	 * determined by the {@link Query} type
	 * 
	 * @param query type-specific list of {@link BusinessObject}
	 * @return List&lt;{@link BusinessObject}&gt;
	 */
	public static <PK,T extends BusinessObject<PK>> List<T> execute(Query<T> query) {
		return getService().executeQuery(query);
	}
	
	/**
	 * This methods executes a {@link Query} and returns a single result {@link BusinessObject} of the type
	 * determined by the {@link Query} type.
	 * Returns null if nothing found and throws exception if query returns more than one row.
	 * 
	 * @param query type-specific {@link BusinessObject}
	 * @return {@link BusinessObject};
	 * @throws BusinessException
	 */
	public static <PK,T extends BusinessObject<PK>> T executeSingleResult(Query<T> query) throws BusinessException {
		return getService().executeQuerySingleResult(query);
	}
	
	/**
	 * This method returns one single {@link BusinessObject} that can be found for the given
	 * Id; Parameter Class&lt;T&gt; type is necessary to ensure type-safety
	 * 
	 * @param type - type-specific class
	 * @param id - Id of the element to search for in the database
	 * @return type-specific - {@link BusinessObject}
	 */
	public static <PK,T extends BusinessObject<PK>> T getById(Class<T> type, PK id) {
		return getService().getById(type, id);
	}
	
	/**
	 * This method returns all {@link BusinessObject}s that contain one of the given processes;<br>A {@link Process} is always assigned to one
	 * entity, so the type of the BusinessObject does not have to be mentioned explicitly;<br>There must be at least one process
	 * @param process
	 * @param additionalProcesses
	 * @return
	 * @throws BusinessException
	 */
	public static <PK, T extends Stateful & BusinessObject<PK>> List<T> getByProcess(Process<T> process, Process<T>... additionalProcesses) throws BusinessException {
		return getService().getByProcess(process, additionalProcesses);
	}
	
	/**
	 * This method returns all {@link BusinessObject}s that contain one of the given states;<br>A {@link State} is always assigned to one
	 * statemodel, that can be used by several entities. So the type of the BusinessObject must be mentioned explicitly;<br>There must be at least one state
	 * @param type
	 * @param state
	 * @param additionalStates
	 * @return
	 * @throws BusinessException
	 */
	public static <PK, T extends Stateful & BusinessObject<PK>> List<T> getByState(Class<T> type, State state, State... additionalStates) throws BusinessException {
		return getService().getByState(type, state, additionalStates);
	}
}
