//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.provider;

import java.util.Map;

import org.nuclos.api.common.NuclosFile;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.locale.NuclosLocale;
import org.nuclos.api.printout.Printout;
import org.nuclos.api.report.OutputFormat;
import org.nuclos.api.service.PrintoutService;

/**
 * {@link PrintoutProvider} provides methods for executing printouts
 * <p>
 * This Class is usually used in Rule-classes and allows API access only
 * </p>
 * @author Matthias Reichart
 */
public class PrintoutProvider {

	private static PrintoutService service;
	
	public void setPrintoutService(PrintoutService repService) {
		this.service = repService;
	}
	
	private static PrintoutService getService() {
		if (service == null) {
			throw new IllegalStateException("too early (missing in spring context?)");
		}
		return service;
	}
	
	/**
	 * This method runs the {@link Printout} that belongs to the given {@link OutputFormat} and 
	 * returns the generated {@link NuclosFile}
	 * 
	 * @param format {@link OutputFormat} - OutputFormat stored in the generated printout class
	 * @return {@link NuclosFile}
	 * 
	 */
	public static NuclosFile run(OutputFormat format, Long boId) 
			throws BusinessException {
		return getService().run(format, boId);
	}
	

	/**
	 * This method runs the {@link Printout} that belongs to the given {@link OutputFormat} and 
	 * returns the generated {@link NuclosFile} containing all localized data in the given locale language 
	 * 
	 * @param format {@link OutputFormat} - OutputFormat stored in the generated printout class
	 * @param locale {@link NuclosLocale} - Language in which the localized data is displayed
	 * @return {@link NuclosFile}
	 * 
	 */
	public static NuclosFile run(OutputFormat format, Long boId, NuclosLocale locale) 
			throws BusinessException {
		return getService().run(format, boId, locale);
	}
	
	
	/**
	 * Providing the parameters this method runs the {@link Printout} that belongs to the given {@link OutputFormat} and 
	 * returns the generated {@link NuclosFile}
	 *
	 * @param format {@link OutputFormat} - OutputFormat stored in the generated printout class
	 * @param boId
	 * @param params {@link Map} - Parameters for the printout
	 * @return {@link NuclosFile}
	 * 
	 */
	public static NuclosFile run(OutputFormat format, Long boId, Map<String, Object> params) 
			throws BusinessException {
		return getService().run(format, boId, params);
	}
	
	/**
	 * Providing the parameters this method runs the {@link Printout} that belongs to the given {@link OutputFormat} and 
	 * returns the generated {@link NuclosFile} containing all localized data in the given locale language 
	 *
	 * @param format {@link OutputFormat} - OutputFormat stored in the generated printout class
	 * @param boId
	 * @param params {@link Map} - Parameters for the printout
	 * @param locale {@link NuclosLocale} - Language in which the localized data is displayed
	 * @return {@link NuclosFile}
	 * 
	 */
	public static NuclosFile run(OutputFormat format, Long boId, Map<String, Object> params, NuclosLocale locale) 
			throws BusinessException {
		return getService().run(format, boId, params, locale);
	}
}
