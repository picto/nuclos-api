//Copyright (C) 2012  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.provider;

import org.nuclos.api.businessobject.BusinessObject;
import org.nuclos.api.businessobject.Query;
import org.nuclos.api.businessobject.facade.thin.Stateful;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.service.StatemodelService;
import org.nuclos.api.statemodel.State;

/**
 * {@link StatemodelProvider} provides methods for reading, setting or changing state information
 * for {@link BusinessObject}s. 
 * 
 * @see Query
 * @see BusinessObject
 * @author Matthias Reichart
 */
public class StatemodelProvider {

	private static StatemodelService SERVICE;
	
	StatemodelProvider() {
	}
	
	public void setStatemodelService(StatemodelService service) {
		this.SERVICE = service;
	}
	
	private static StatemodelService getService() {
		if (SERVICE == null) {
			throw new IllegalStateException("too early (missing in spring context?)");
		}
		return SERVICE;
	}

	/**
	 * This method changes the state of a given {@link BusinessObject}.
	 * <p>
	 * The passed numeral will be checked wether it can be assigned to the {@link BusinessObject}
	 * or not; <p>In case of an invalid numeral the method will throw an exception. 
	 * 
	 * @param t - {@link BusinessObject} that state must be changed
	 * @param iNumeral - The Numeral of the new state
	 * @throws Exception
	 * 
	 * @deprecated - Please use method changeState(T t, State sState) to ensure type-safety
	 */
	@Deprecated
	public static <T extends Stateful> void changeState(T t, int iNumeral) throws Exception {
		getService().changeState(t, iNumeral);
	}
	
	/**
	 * This method changes the state of a given {@link BusinessObject}.
	 * <p>
	 * The passed numeral is type-safe. It is checked wether the state is valid for the statemodel.
	 * <p>In case of an invalid numeral or a non-fitting state/statemodel an BusinessException will be thrown back. 
	 * 
	 * @param t - {@link BusinessObject} that state must be changed
	 * @param sState
	 * @throws BusinessException
	 */
	public static <T extends Stateful> void changeState(T t, State sState) throws BusinessException {
		getService().changeState(t, sState);
	}
	
}
