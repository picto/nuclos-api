//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.provider;

import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.service.UserService;
import org.nuclos.api.service.WebService;
import org.nuclos.api.ws.WebServiceObject;

/**
 * The {@link UserService} provides several methods to create users and manipulate user role data
 * @author reichama
 *
 */
public class WebServiceProvider {

	private static WebService SERVICE;
	
	WebServiceProvider() {}
	
	public void setWebservice(WebService service) {
		this.SERVICE = service;
	}
	
	private static WebService getService() {
		if (SERVICE == null) {
			throw new IllegalStateException("too early (missing in spring context?)");
		}
		return SERVICE;
	}
	
	/**
	 * This method creates a new instance of the given stub class and provides it with all settings (e.g. Proxy, Authentication)
	 * that are deposited for it by the administration within Nuclos.<br>
	 * 
	 * {@link WebServiceObject} 
	 * 
	 * @param Class<T> stub class
	 * @param WebServiceObject webService
	 * @return T instancea of stub class
	 * @throws BusinessException
	 */
	public static <T> T getStub(Class<T> stubClass, WebServiceObject webServiceUID) throws BusinessException {
		return getService().getStub(stubClass, webServiceUID);
	}
}
