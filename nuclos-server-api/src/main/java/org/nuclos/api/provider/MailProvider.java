//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.provider;

import java.util.List;

import org.nuclos.api.common.NuclosFile;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.mail.NuclosMail;
import org.nuclos.api.service.MailService;

/**
 * This class is used in rule programming and provides methods for sending and receiving
 * E-mails. <p> Please check <a href="http://wiki.nuclos.de/display/Administration/Systemparameter">Nuclos system parameter</a> for setting connection parameter like POP3/SMTP 
 * </p>
 * @author Matthias Reichart
 */
public class MailProvider {

	private static MailService service;
	
	public void setMailService(MailService repService) {
		this.service = repService;
	}
	
	@SuppressWarnings("static-access")
	private static MailService getService() {
		if (service == null) {
			throw new IllegalStateException("too early (missing in spring context?)");
		}
		return service;
	}
	
	/**
	 * This method sends a {@link NuclosMail}
	 * All connection settings like POP3/SMTP must be set in Nuclos as system parameters
	 * 
	 * @param mail {@link NuclosMail} - email to send
	 * 
	 */
	public static void send(NuclosMail mail) 
			throws BusinessException {
		getService().send(mail);
	}

	/**
	 * This method retrieves all {@link NuclosMail}s that can be found in the Inbox
	 * of the account mentioned in the system parameters.
	 * <p>
	 * If argument 'bDeleteMails' is true, all mails will be deleted after retrieving them from the server.
	 * <p>
	 * All connection settings like POP3/SMTP must be set in Nuclos as system parameters
	 * 
	 * @param bDeleteMails - bDeleteMails
	 * @return List of {@link NuclosFile}
	 * 
	 */
	public static List<NuclosMail> receive(boolean bDeleteMails) 
			throws BusinessException {
		return getService().receive(bDeleteMails);
	}


	/**
	 * This method retrieves all {@link NuclosMail}s that can be found in the specified folder
	 * of the account mentioned in the system parameters.
	 * <p>
	 * Fetching mails from specific folders only works via IMAP. For POP3 the folder is always "INBOX".
	 * <p>
	 * If argument 'bDeleteMails' is true, all mails will be deleted after retrieving them from the server.
	 * <p>
	 * All connection settings like POP3/SMTP must be set in Nuclos as system parameters
	 * 
	 * @param folderFrom 
	 * @param bDeleteMails - bDeleteMails
	 * @return List of {@link NuclosFile}
	 * 
	 */
	public static List<NuclosMail> receive(String folderFrom, boolean bDeleteMails) 
			throws BusinessException {
		return getService().receive(folderFrom, bDeleteMails);
	}
}
