package org.nuclos.api.provider;

import java.util.Date;

import org.nuclos.api.common.NuclosRole;
import org.nuclos.api.common.NuclosUser;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.service.UserService;

/**
 * The {@link UserProvider} provides several methods to create users and manipulate user role data
 * @author reichama
 *
 */
public class UserProvider {

	private static UserService service;
	
	public void setUserService(UserService repService) {
		this.service = repService;
	}
	
	private static UserService getService() {
		if (service == null) {
			throw new IllegalStateException("too early (missing in spring context?)");
		}
		return service;
	}

	/**
	 * This method creates a Nuclos User and returns the id of the new object. Password is created by Nuclos automatically.
	 * Afterwards a notification Email containing the password is sent to the user.<br> 
	 * <b>Note</b>: A new user does not have any roles and therefore does not have any rights in Nuclos. Use method addRole() and removeRole() for modifications
	 * 
	 * @param username, firstname, lastname, email, passwordChangeRequired
	 * @return UID
	 * @throws BusinessException
	 */
	public static org.nuclos.api.UID insert(String username, String firstname, String lastname, String email, Boolean passwordChangeRequired) throws BusinessException {
		return getService().insert(username, firstname, lastname, email, passwordChangeRequired);
	}
	
	/**
	 * This method creates a Nuclos User and returns the id of the new object. A password must be provided.
	 * Afterwards a notification Email containing the password is sent to the user.<br> 
	 * <b>Note</b>: A new user does not have any roles and therefore does not have any rights in Nuclos. Use method addRole() and removeRole() for modifications
	 * 
	 * @param username, firstname, lastname, email, password, passwordChangeRequired
	 * @return UID
	 * @throws BusinessException
	 */
	public static org.nuclos.api.UID insert(String username, String firstname, String lastname, String email, String password, Boolean passwordChangeRequired) throws BusinessException {
		return getService().insert(username, firstname, lastname, email, password, passwordChangeRequired);
	}

	/**
	 * This method assigns a role to a given user. The class of the user role is generated automatically and
	 * corresponds to an existing user role in Nuclos. The {@link NuclosUser} can be extracted by using {@link QueryProvider} or
	 * {@link BusinessObjectProvider} 
	 * 
	 * @param role
	 * @param user
	 * @return
	 * @throws BusinessException
	 */
	public static org.nuclos.api.UID grantRole(Class<? extends NuclosRole> role, NuclosUser user) throws BusinessException {
		return getService().grantRole(role,user);
	}
	
	/**
	 * This method dispossesses a user of the given role. The class of the user role is generated automatically and
	 * corresponds to an existing user role in Nuclos. The {@link NuclosUser} can be extracted by using {@link QueryProvider} or
	 * {@link BusinessObjectProvider}. If the user is not assigned to the role a {@link BusinessException} is thrown.
	 * 
	 * @param role
	 * @param user
	 * @throws BusinessException
	 */
	public static void revokeRole(Class<? extends NuclosRole> role, NuclosUser user) throws BusinessException {
		getService().revokeRole(role,user);
	}
	
	/**
	 * This method allows to define a date on which to expire a specific user account. 
	 * The {@link NuclosUser} can be extracted by using {@link QueryProvider} or
	 * {@link BusinessObjectProvider}.
	 * 
	 * @param user
	 * @param date
	 * @throws BusinessException
	 */
	public static void expire(NuclosUser user, Date date) throws BusinessException {
		getService().expire(user,date);
	}
	
	
}
