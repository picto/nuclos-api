package org.nuclos.api.provider;

import org.nuclos.api.communication.CommunicationPort;
import org.nuclos.api.communication.CommunicationPortKey;
import org.nuclos.api.context.communication.InstantiableContext;
import org.nuclos.api.context.communication.NotificationContext;
import org.nuclos.api.context.communication.RequestContext;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.service.CommunicationService;

/**
 * {@link CommunicationProvider} provides methods for communication with ports
 * <p>
 * This Class is usually used in Rule-classes and allows API access only
 * </p>
 */
public class CommunicationProvider {

	private static CommunicationService service;
	
	CommunicationProvider() {
	}
	
	public void setCommunicationService(CommunicationService comService) {
		this.service = comService;
	}
	
	private static CommunicationService getService() {
		if (service == null) {
			throw new IllegalStateException("too early (missing in spring context?)");
		}
		return service;
	}
	
	/**
	 * 
	 * @param context
	 * @throws BusinessException
	 */
	public static void handleNotification(NotificationContext context) throws BusinessException {
		service.handleNotification(context);
	}
	
	/**
	 * 
	 * @param context
	 * @throws BusinessException
	 */
	public static void executeRequest(RequestContext<?> context) throws BusinessException {
		service.executeRequest(context);
	}
	
	/**
	 * 
	 * @param context
	 * @throws BusinessException
	 */
	public static void handleResponse(RequestContext<?> context) throws BusinessException {
		service.handleResponse(context);
	}
	
	/**
	 * For use in rules.
	 * @param portKey
	 * @param contextClass
	 * @return
	 * 		a new instance of the given context class 
	 * @throws BusinessException 
	 */
	public static <T extends InstantiableContext> T newContextInstance(CommunicationPortKey portKey, Class<T> contextClass) throws BusinessException {
		return service.newContextInstance(portKey, contextClass);
	}
	
	/**
	 * For internal use in ports.
	 * @param port
	 * @param contextClass
	 * @return
	 * 		a new instance of the given context class 
	 * @throws BusinessException 
	 */
	public static <T extends InstantiableContext> T newContextInstance(CommunicationPort port, Class<T> contextClass) throws BusinessException {
		return service.newContextInstance(port, contextClass);
	}
	
}
