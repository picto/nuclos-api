//Copyright (C) 2012  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.provider;

import java.util.Collection;

import org.nuclos.api.NuclosImage;
import org.nuclos.api.businessobject.BusinessObject;
import org.nuclos.api.businessobject.facade.LogicalDeletable;
import org.nuclos.api.common.NuclosFile;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.service.BusinessObjectService;

/**
 * {@link BusinessObjectProvider} provides methods for creating, updating and deleting new or existing BusinessObjects
 * <p>
 * This Class is usually used in Rule-classes and allows API access only
 * </p>
 * @author Matthias Reichart
 */
public class BusinessObjectProvider{

	private static BusinessObjectService SERVICE;
	
	BusinessObjectProvider() {
	}
	
	public void setBusinessObjectService(BusinessObjectService service) {
		this.SERVICE = service;
	}
	
	private static BusinessObjectService getService() {
		if (SERVICE == null) {
			throw new IllegalStateException("too early (missing in spring context?)");
		}
		return SERVICE;
	}
	
	/**
	 * This method receives a new BusinessObject and stores it in the database.
	 * If the BusinessObject is not flagged 'new' a BusinessException is thrown.
	 * @param type
	 * @throws BusinessException
	 */
	public static <T extends BusinessObject> Long insert(T type) throws BusinessException {
		return getService().insert(type);
	}
	
	/**
	 * This method receives a collection of new BusinessObjects and stores them in the database.
	 * If one BusinessObject is not flagged 'new' a BusinessException is thrown.
	 * @param type
	 * @throws BusinessException
	 */
	public static <T extends BusinessObject> Collection<Long> insertAll(Collection<T> type) throws BusinessException {
		return getService().insertAll(type);
	}
	
	
	/**
	 * This method updates a BusinessObjects in the database.
	 * If the BusinessObject is not flagged 'update' a BusinessException is thrown.
	 * @param type
	 * @throws BusinessException
	 */
	public static <T extends BusinessObject> void update(T type) throws BusinessException {
		getService().update(type);
	}
	
	/**
	 * This method updates all BusinessObjects in the given collection.
	 * If one BusinessObject is not flagged 'update' a BusinessException is thrown.
	 * @param type
	 * @throws BusinessException
	 */
	public static <T extends BusinessObject> void updateAll(Collection<T> type) throws BusinessException {
		getService().updateAll(type);
	}
	
	/**
	 * This method deletes the given BusinessObject from the database
	 * @param type
	 * @throws BusinessException
	 */
	public static <T extends BusinessObject> void delete(T type) throws BusinessException {
		getService().delete(type);
	}
	
	/**
	 * This method deletes all BusinessObjects of then given collection from the database.
	 * @param type
	 * @throws BusinessException
	 */
	public static <T extends BusinessObject> void deleteAll(Collection<T> type) throws BusinessException {
		getService().deleteAll(type);
	}
	
	
	/**
	 * This method deletes all BusinessObjects logical without removing data from the database.
	 * @param type
	 * @throws BusinessException
	 */
	public static <PK, T extends LogicalDeletable> void deleteLogical(T type) throws BusinessException {
		getService().deleteLogical(type);
	}

	/**
	 * This method deletes all BusinessObjects logical without removing data from the database.
	 * @param type
	 * @throws BusinessException
	 */
	public static <PK, T extends LogicalDeletable> void deleteLogicalAll(Collection<T> type) throws BusinessException {	
		getService().deleteLogicalAll(type);
	}
	
	/**
	 * This method creates a new {@link NuclosFile} using the given content
	 * 
	 * @param name
	 * @param content
	 * @return {@link NuclosFile}
	 * @throws BusinessException
	 */
	public static NuclosFile newFile(String name, byte[] content) throws BusinessException {
		return getService().newFile(name, content);
	}
	
	/**
	 * This method creates a new {@link NuclosImage} using the given content;
	 * a thumbnail will be created automatically
	 * @param name
	 * @param content
	 * @return {@link NuclosImage}
	 * @throws BusinessException
	 */
	public static NuclosImage newImage(String name, byte[] content) throws BusinessException {
		return getService().newImage(name, content);
	}
	
}
