package org.nuclos.api.provider;

import java.util.List;

import org.nuclos.api.businessobject.BusinessObject;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.generation.Generation;
import org.nuclos.api.service.GenerationService;

/**
 * {@link GenerationProvider} provides methods for executing nuclos generations
 * <p>
 * This Class is usually used in Rule-classes and allows API access only
 * </p>
 * @author Matthias Reichart
 */
public class GenerationProvider {

	private static GenerationService service;
	
	GenerationProvider() {
	}
	
	public void setGenerationService(GenerationService genService) {
		this.service = genService;
	}
	
	private static GenerationService getService() {
		if (service == null) {
			throw new IllegalStateException("too early (missing in spring context?)");
		}
		return service;
	}
	
	/**
	 * Using the source object this method runs the given generation and returns the new created target object.  
	 * The generation is type-safe, which determines the type of the source and the target object
	 * 
	 * @param s - type-specific source-object
	 * @param genClass Generation&lt;S,T&gt; - type-specific generation
	 * 
	 * @return T - target-object
	 * 
	 * @exception BusinessException
	 */
	public static <S extends BusinessObject, T extends BusinessObject> T 
			execute(S s, Class<? extends Generation<S,T>> genClass) throws BusinessException {
		return getService().execute(s, genClass);
	}
	
	/**
	 * Using the source objects this method runs the given generation and returns the new created target objects.  
	 * The generation is type-safe, which determines the type of the source and the target objects. 
	 * <br><b>Note</b>: If the generation object enables 'group by'-functionality, all grouping-arguments will 
	 * be used. Otherwise for each source a target will be created and returned in the result list.
	 * 
	 * @param s List&lt;S&gt; s - type-specific source-object
	 * @param genClass Generation&lt;S,T&gt; genClass - type-specific generation
	 * 
	 * @return List&lt;T&gt; - List of target-objects
	 * 
	 * @exception BusinessException
	 */
	public static <S extends BusinessObject, T extends BusinessObject> List<T> 
			execute(List<S> s, Class<? extends Generation<S,T>> genClass) throws BusinessException {
		return getService().execute(s, genClass);
	}
}
