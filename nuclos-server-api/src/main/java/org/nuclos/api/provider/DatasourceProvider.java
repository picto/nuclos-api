//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.provider;

import java.util.Map;

import org.nuclos.api.datasource.Datasource;
import org.nuclos.api.datasource.DatasourceResult;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.service.DatasourceService;

/**
 * {@link DatasourceProvider} provides methods for executing datasource-queries
 * <p>
 * This Class is usually used in Rule-classes and allows API access only
 * </p>
 * @see DatasourceResult
 * @see Datasource
 * @author Matthias Reichart
 */
public class DatasourceProvider {
	
	private static DatasourceService SERVICE;
	
	DatasourceProvider() {}
	
	public void setDatasourceService(DatasourceService service) {
		this.SERVICE = service;
	}
	
	private static DatasourceService getService() {
		if (SERVICE == null) {
			throw new IllegalStateException("too early (missing in spring context?)");
		}
		return SERVICE;
	}
	
	/**
	 * This methods executes a query for a given {@link Datasource}-Object. The returning NuclosResult
	 * contains all found results. There are no parameter passed to the query.
	 * 
	 * @param datasourceClass Class&lt;? extends Datasource&gt; datasourceClass
	 * @return NuclosResult
	 */
	public static DatasourceResult run(Class<? extends Datasource> datasourceClass) throws BusinessException {
		return getService().run(datasourceClass);
	}
	
	/**
	 * This methods executes a query for a given {@link Datasource}-Object. The returning NuclosResult
	 * contains all found results. The parameters are passed to the query.
	 * 
	 * @param datasourceClass Class&lt;? extends Datasource&gt; datasourceClass
	 * @param params Map&lt;String, Object&gt; params
	 * @return NuclosResult
	 */
	public static DatasourceResult run(Class<? extends Datasource> datasourceClass, Map<String, Object> params) throws BusinessException {
		return getService().run(datasourceClass, params);
	}
}
