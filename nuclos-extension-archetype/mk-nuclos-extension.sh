#!/bin/bash

if [ $# -eq 0 ]; then
	echo "Usage: $0 <groupId> [<artifactId> [<version>]]"
	exit -1
fi

GROUPID=$1
ARTIFACTID=$2
VERSION=$3

if [ -z "$GROUPID" ]; then
	GROUPID='mygroup'
fi
if [ -z "$ARTIFACTID" ]; then
	ARTIFACTID='mynuclosextension'
fi
if [ -z "$VERSION" ]; then
	VERSION='1.0.0-SNAPSHOT'
fi

mvn archetype:generate				\
  -DarchetypeGroupId=de.novabit			\
  -DarchetypeArtifactId=nuclos-extension	\
  -DarchetypeVersion=2.0.0-SNAPSHOT		\
  -DgroupId=$GROUPID				\
  -DartifactId=$ARTIFACTID			\
  -Dversion=$VERSION
