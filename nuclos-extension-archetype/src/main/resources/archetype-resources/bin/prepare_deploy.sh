#!/bin/bash -x

# for newly created self-signed key
KEYSTORE_DNAME="cn=Firstname Lastname, ou=Unit, o=Novabit, c=DE"
KEYSTORE_KEYALIAS="extension"
KEYSTORE_STOREPASSWD="nuclos"
KEYSTORE_KEYPASSWD="nuclos"

# Do not edit beneath this line 
REGEX_DEP="^(.+):(.+):(.+):(.+):(.+)$"
MAVEN_REPOSITORY="$HOME/.m2/repository"

INSTALL_DIR=.
#INSTALL_DIR=$1
#if [ ! -d $INSTALL_DIR ]; then
#	echo "Usage: $0 <nuclos_install_dir>" 
#	exit -2
#fi
#shift

#if [ ! -d "$INSTALL_DIR/webapp/app" ]; then
#	echo "Unlikely that $INSTALL_DIR is a nuclos installation directory"
#	exit -2
#fi

EXTENSION_DIR=`pwd`
REGEX_IGNORE="(aspectjtools|nuclos-common-api|nuclos-client-api)-.*jar"

copyDeps() {
	local DIR="$1"
	shift
	local COPY_DIR="$1"
	shift
	pushd "$DIR"
		rm -f "list.txt"
		# https://maven.apache.org/plugins/maven-dependency-plugin/list-mojo.html
		mvn dependency:list -DincludeScope=runtime -DoutputFile=list.txt >>/dev/null
	popd
	for i in `cat $DIR/list.txt`; do
		if [[ "$i" =~ $REGEX_DEP ]]; then
			echo "$i"
			local GROUPID="${BASH_REMATCH[1]}"
			local ARTEFACTID="${BASH_REMATCH[2]}"
			local TYPE="${BASH_REMATCH[3]}"
			local VERSION="${BASH_REMATCH[4]}"
			local SCOPE="${BASH_REMATCH[5]}"
			local PATH=`echo "$GROUPID" | /bin/sed -e 's/\./\//g'`
			local FILE_DEP="$MAVEN_REPOSITORY/$PATH/$ARTEFACTID/$VERSION/$ARTEFACTID-$VERSION.jar"
			# echo "$GROUPID -> $PATH"
			# echo "$FILE_DEP"
			# ls "$FILE_DEP"
			/bin/cp "$FILE_DEP" "$COPY_DIR"
		fi
	done
	/bin/rm -f "$DIR/list.txt"
}

processDeps() {
	local DIR="$1"
	shift
	local COPY_DIR="$1"
	shift
	local TARGET_DIR="$1"
	shift
	copyDeps "$DIR" "$COPY_DIR"
	pushd "$COPY_DIR"
		for i in *.jar; do
			if [[ "$i" =~ $REGEX_IGNORE ]]; then
				continue;
			fi
			#if [[ ! ( -f "$INSTALL_DIR/webapp/app/$i" || -f "$INSTALL_DIR/webapp/app/$i.pack.gz" ) ]]; then
			if grep "$INSTALL_DIR/webapp/app/$i" $0/../app.list; then
				unsign "$i"
				sign "$i"
				echo "cp $i $TARGET_DIR"
				cp "$i" "$TARGET_DIR"
			fi
		done
	popd
}

unsign() {
        local JAR="$1"
        shift
        zip -q -d "$JAR" META-INF/\*.SF META-INF/\*.DSA META-INF/\*.RSA META-INF/INDEX.LIST >>/dev/null
        # META-INF/MANIFEST.MF
}

sign() {
        local JAR="$1"
        shift
        jarsigner -keystore "$KEYSTORE_FILE" -storepass "$KEYSTORE_STOREPASSWD" \
		-keypass "$KEYSTORE_KEYPASSWD" "$JAR" "$KEYSTORE_KEYALIAS" >>/dev/null
}

KEYSTORE_FILE="$EXTENSION_DIR/key"
# Check for 'official' key store
if [[ -n "$KEYSTORE" ]]; then
	KEYSTORE_KEYALIAS="$ALIAS"
	KEYSTORE_STOREPASSWD="$STOREPASS"
	KEYSTORE_KEYPASSWD="$KEYPASS"
	cp "$KEYSTORE" "$KEYSTORE_FILE"
fi
# Create keystore if needed
if [ ! -f "$KEYSTORE_FILE" ]; then
	keytool -genkeypair -dname "$KEYSTORE_DNAME" \
		-alias "$KEYSTORE_KEYALIAS" -keypass "$KEYSTORE_KEYPASSWD" \
		-keystore "$KEYSTORE_FILE" -storepass "$KEYSTORE_STOREPASSWD" \
		-validity 180 || exit -2
fi

# http://wiki.nuclos.de/display/Konfiguration/Entwicklungsumgebung
pushd "$INSTALL_DIR"
	rm -rf "extensions"
	mkdir -p "extensions/common/native"
	mkdir -p "extensions/client/themes"
	mkdir -p "extensions/server"
popd

COMMON_DIR=`ls -d *-common`
CLIENT_DIR=`ls -d *-client`
SERVER_DIR=`ls -d *-server`
pushd "$COMMON_DIR/target"
	JAR=`ls *-common-*.jar`
	unsign "$JAR"
	sign "$JAR"
popd
pushd "$CLIENT_DIR/target"
	JAR=`ls *-client-*.jar`
	unsign "$JAR"
	sign "$JAR"
popd

cp *-common/target/*-common-*.jar "$INSTALL_DIR/extensions/common"
cp *-client/target/*-client-*.jar "$INSTALL_DIR/extensions/client"
cp *-theme*/target/*-theme*-*.jar "$INSTALL_DIR/extensions/client/themes"
cp *-server/target/*-server-*.jar "$INSTALL_DIR/extensions/server"

mkdir deps
processDeps "$COMMON_DIR" deps "$INSTALL_DIR/extensions/common"

rm -rf deps
mkdir deps
processDeps $CLIENT_DIR deps "$INSTALL_DIR/extensions/client"
rm -f $INSTALL_DIR/extensions/client/*-common-*.jar

rm -rf deps

tree "$INSTALL_DIR/extensions"
