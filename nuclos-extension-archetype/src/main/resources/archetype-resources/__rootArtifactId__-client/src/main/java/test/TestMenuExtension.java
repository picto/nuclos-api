package tp.test;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;

import org.nuclos.api.ui.MenuItem;
import org.nuclos.api.ui.annotation.NucletComponent;
import org.springframework.stereotype.Component;

@NucletComponent
public class TestMenuExtension implements MenuItem {
	
	public TestMenuExtension() {
		System.err.println("TestMenu");
	}

	@Override
	public String getLabel() {
		return "TestMenuExtension";
	}

	@Override
	public Action getAction() {
		return new AbstractAction("TestMenuExtensionAction") {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				System.err.println("TestMenuExtensionAction fired");
			}
		};
	}

	@Override
	public String[] getMenuPath() {
		return new String[] { "extension" };
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

}
