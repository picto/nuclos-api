package org.nuclos.api.ui;

import javax.swing.Action;

public interface MailToEventHandler {

	public Action getMailToAction(String email, String name);
	
}
