//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.ui.layout;

import javax.swing.Icon;

import org.nuclos.api.context.LayoutComponentContext;
import org.nuclos.api.ui.Alignment;

public interface LayoutComponentFactory<PK> {
	
	/**
	 * creates new instances
	 * @param context	layout component context {@link LayoutComponentContext}
	 * @return
	 */
	public LayoutComponent<PK> newInstance(LayoutComponentContext context);

	/**
	 * 
	 * @return name shown in layout editor palette
	 */
	public String getName();
	
	/**
	 * 
	 * @return icon shown in layout editor palette
	 */
	public Icon getIcon();
	
	/**
	 * is used by layout editor
	 * @return the default alignment for this component
	 * <code>null</code> uses fallback <code>new Alignment(AlignmentH.FULL, AlignmentV.CENTER)</code>
	 */
	public Alignment getDefaulAlignment();
	
	/**
	 * is used by layout editor
	 * @return the default for a property
	 * <code>null</code> is allowed
	 */
	public Object getDefaultPropertyValue(String property);
	
}
