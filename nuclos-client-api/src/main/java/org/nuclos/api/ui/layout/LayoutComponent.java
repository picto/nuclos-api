//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.ui.layout;

import java.awt.Dimension;
import java.util.Collection;
import java.util.EnumSet;

import javax.swing.JComponent;
import javax.swing.border.Border;

import org.nuclos.api.Preferences;
import org.nuclos.api.Property;
import org.nuclos.api.context.LayoutComponentContext;

public interface LayoutComponent<PK> {
	
	/**
	 * 
	 * layout component type
	 *
	 */
	public static enum LayoutComponentType {
		/**
		 * Design - WYSIWYG-Editor
		 */
		DESIGN,
		
		/**
		 * Detail - running layout
		 */
		DETAIL;
	}

	/**
	 * 
	 * @param type the type {@link LayoutComponentType} of the requested component
	 * @return component
	 */
	JComponent getComponent(LayoutComponentType type);

	/**
	 * get set of supported {@link LayoutComponentType}
	 * 
	 * @return
	 */
	EnumSet<LayoutComponentType> getSupportedTypes();
	
	/**
	 * 
	 * @param prefs 
	 * 	also for restoring state from preferences
	 */
	void setPreferences(Preferences prefs);
	
	/**
	 * 
	 * @param name
	 */
	void setName(String name);
	
	/**
	 * 
	 * @return name for component
	 */
	String getName();
	
	/**
	 * 
	 * @param border
	 */
	void setBorder(Border border);
	
	/**
	 * 
	 * @param preferredSize
	 */
	void setPreferredSize(Dimension preferredSize);
	
	/**
	 * 
	 * @param name
	 * @param value
	 */
	void setProperty(String name, Object value);
	
	/**
	 * Give the layout component a handle for controlling some aspects of
	 * the tab it is displayed on.
	 * 
	 * @param tabController control handle
	 * 
	 * @see TabController
	 * @since Nuclos 4.3.2
	 * @author Thomas Pasch
	 */
	void setTabController(TabController tabController);
	
	/**
	 * 
	 * @return 
	 * <code>null</code> if no additional properties are required.
	 * Otherwise return array of <code>Property</code>.
	 * 
	 * supported types are:
	 * 		<code>java.lang.Boolean</code>
	 * 		<code>java.lang.String</code>
	 * 		<code>java.lang.Integer</code>
	 * 
	 * 		<code>java.awt.Dimension</code>
	 * 		<code>java.awt.Color</code>
	 * 		<code>java.awt.Font</code>
	 * 
	 * 		<code>org.nuclos.common.NuclosScript</code>
	 * 		<code>org.nuclos.common.NuclosTranslationMap</code>
	 * 		<code>org.nuclos.common.NuclosValuelistProvider</code>
	 * 
	 */
	Property[] getComponentProperties();
	
	/**
	 * 
	 * @return 
	 * <code>null</code> if no additional properties are required.
	 * Otherwise return <code>String[]</code>
	 */
	String[] getComponentPropertyLabels();
	
	/**
	 * @return context {@link LayoutComponentContext} for layout component
	 */
	LayoutComponentContext getContext();
	
	/**
	 * get the listeners for layout components
	 * 
	 * @return
	 */
	Collection<LayoutComponentListener<PK>> getLayoutComponentListeners();
	
	/**
	 * add new {@link LayoutComponentListener} to component
	 * 
	 * @param listener the {@link LayoutComponentListener}
	 */
	void addLayoutComponentListener(LayoutComponentListener<PK> listener);
	
	/**
	 * remove {@link LayoutComponentListener} from component
	 * 
	 * @param listener the {@link LayoutComponentListener}
	 */
	void removeLayoutComponentListener(LayoutComponentListener<PK> listener);
}
