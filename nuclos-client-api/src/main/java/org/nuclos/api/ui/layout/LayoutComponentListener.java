//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.ui.layout;

import org.nuclos.api.context.MultiContext;
import org.nuclos.api.context.SingleContext;

/**
 * Layout Component Listener
 * 
 * listener for events thrown by the user interface
 * 
 * @author Moritz Neuhäuser &lt;moritz.neuhaeuser@nuclos.de&gt;
 *
 */
public interface LayoutComponentListener<PK> {

	/**
	 * search entered
	 * 
	 * @param context {@link SearchContext}
	 */
	void searchEntered(SearchContext context);
	
	/**
	 * new entered 
	 * 
	 * @param context {@link NewContext}
	 */
	void newEntered(NewContext context);
	
	/**
	 * view entered by single object
	 * 
	 * @param context {@link SingleContext}
	 */
	void singleViewEntered(SingleContext<PK> context);
	
	/**
	 * view entered by multiple objects
	 * 
	 * @param context {@link MultiContext}
	 */
	void multiViewEntered(MultiContext<PK> context);
	
	/**
	 * delete entered by single object (before)
	 * 
	 * @param context {@link SingleContext}
	 */
	void singleDeleteBefore(SingleContext<PK> context);
	
	/**
	 * delete entered by single object (after)
	 * 
	 * @param context {@link SingleContext}
	 */
	void singleDeleteAfter(SingleContext<PK> context);
	
	/**
	 * delete entered by multiple objects (before)
	 * 
	 * @param context {@link MultiContext}
	 */
	void multiDeleteBefore(MultiContext<PK> context);
	
	/**
	 * delete entered by multiple objects (after)
	 * 
	 * @param context {@link MultiContext}
	 */
	void multiDeleteAfter(MultiContext<PK> context);
	
	
	void singleUpdateBefore(SingleContext<PK> context);
	
	void singleUpdateAfter(SingleContext<PK> context);
	
	void singleInsertBefore(SingleContext<PK> context);
	
	void singleInsertAfter(SingleContext<PK> context);
	
	/**
	 * Intended, not yet supported
	public void singleEditEntered(SingleContext context);
	
	public void multiUpdateBefore(MultiContext context);
	public void multiUpdateAfter(MultiContext context);
	
	public void multiInsertBefore(MultiContext context);
	public void multiInsertAfter(MultiContext context);
	
	public void searchRun(SearchContext context);
	 */
}
