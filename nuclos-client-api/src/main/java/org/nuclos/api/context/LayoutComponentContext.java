//Copyright (C) 2012  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.context;

import org.nuclos.api.UID;
import org.nuclos.api.ui.layout.LayoutComponent.LayoutComponentType;


/**
 * {@link LayoutComponentContext} represents the base context that carries
 * environmental informations for layout components
 *
 * @author Moritz Neuhäuser &lt;moritz.neuhaeuser@nuclos.de&gt;
 */
public interface LayoutComponentContext {
	
	/**
	 * This method supplies the name of the  underlying entity
	 * 
	 * @return entity name
	 */
	UID getEntityUid();
	
	/**
	 * This method supplies the {@link LayoutComponentType}
	 * 
	 * @return layout environment {@link LayoutComponentType}
	 */
	LayoutComponentType getType();

}
