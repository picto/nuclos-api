package org.nuclos.api.context;

import java.util.Collection;

public interface MultiContext<PK> {
	
	Collection<PK> getAllObjectIds();
	
}
