//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.context;

import java.io.Serializable;
import java.util.Map;

import javax.swing.JPanel;

/**
 * Interface for creating input delegate views.
 * Interface is required for data exchange between {@link InputDelegate} implementation and the nuclos core.
 * @author thomas.schiffmann
 */
public interface InputDelegate {
	
	/**
	 * Initialize the input delegate view with optional context data from server.
	 * @param data Context data from server or rule (optional)
	 * @return The {@link JPanel} with the input components the user will see.
	 */
	JPanel initialize(Map<String, Serializable> data);
	
	/**
	 * Validate and collect data from input components.
	 * @return Collected data as map of strings (key) and serializables.
	 * @throws InputValidationException If user input is incomplete or entered values are invalid.
	 */
	Map<String, Serializable> evaluate() throws InputValidationException;
}
