<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<groupId>org.nuclos</groupId>
	<artifactId>nuclos-api</artifactId>
	<version>4.20.0-SNAPSHOT</version>
	<packaging>pom</packaging>

	<name>Nuclos API</name>
	<url>http://www.nuclos.de/</url>
	<description>Nuclos ERP-Baukasten Application Programming Interface.</description>

	<scm>
		<connection>scm:git:https://bitbucket.org/nuclos/nuclos-api.git</connection>
		<developerConnection>scm:git:ssh://git@bitbucket.org:nuclos/nuclos-api.git</developerConnection>
		<url>https://bitbucket.org/nuclos/nuclos-api</url>
		<tag>${version}</tag>
  </scm>

	<issueManagement>
		<system>Jira</system>
		<url>http://support.novabit.de/browse/NUCLOS-API</url>
	</issueManagement>

	<ciManagement>
		<system>Jenkins</system>
		<url>http://192.168.1.116/jenkins/</url>
	</ciManagement>

	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<aspectj.version>1.8.4</aspectj.version>
		<java.version>1.6</java.version>
		<log4j.version>2.3</log4j.version>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<aspectj.phase.1>compile</aspectj.phase.1>
		<aspectj.phase.2>test-compile</aspectj.phase.2>
	</properties>

	<modules>
		<module>nuclos-rigid-api</module>
		<module>nuclos-common-api</module>
		<module>nuclos-server-api</module>
		<module>nuclos-client-api</module>
		<module>nuclos-ide-api</module>
	</modules>

	<dependencies>
		<dependency>
			<groupId>org.aspectj</groupId>
			<artifactId>aspectjrt</artifactId>
		</dependency>
		<!--
			Trying to fix jenkins build
			-->
		<!-- 
			This is only needed because of 
			https://jira.springsource.org/browse/SPR-6819
			-->
		<dependency>
			<groupId>javax.persistence</groupId>
			<artifactId>persistence-api</artifactId>
			<version>1.0</version>
			<scope>provided</scope>
		</dependency>
	</dependencies>
	
	<dependencyManagement>
		<dependencies>
			<dependency>
				<!-- groupId>javax.annotation</groupId> <artifactId>com.springsource.javax.annotation</artifactId> 
					<version>1.0.0</version -->
				<groupId>javax.annotation</groupId>
				<artifactId>jsr250-api</artifactId>
				<version>1.0</version>
			</dependency>
			<dependency>
				<groupId>org.aspectj</groupId>
				<artifactId>aspectjrt</artifactId>
				<version>${aspectj.version}</version>
			</dependency>
			<!-- 
				This is only needed because of 
				https://jira.springsource.org/browse/SPR-6819
			 -->
			<dependency>
				<groupId>javax.persistence</groupId>
				<artifactId>persistence-api</artifactId>
				<version>1.0.2</version>
				<scope>provided</scope>
			</dependency>
			<dependency>
    			<groupId>org.apache.logging.log4j</groupId>
    			<artifactId>log4j-api</artifactId>
    			<version>${log4j.version}</version>
  			</dependency>
  			<dependency>
    			<groupId>org.apache.logging.log4j</groupId>
    			<artifactId>log4j-core</artifactId>
    			<version>${log4j.version}</version>
  			</dependency>
  			<dependency>
  				<!-- Log4j 1.x API Bridge
					If existing components use Log4j 1.x and you want to have this logging routed
					to Log4j 2, then remove any log4j 1.x dependencies and add the following. -->
    			<groupId>org.apache.logging.log4j</groupId>
    			<artifactId>log4j-1.2-api</artifactId>
    			<version>${log4j.version}</version>
  			</dependency>
		</dependencies>
	</dependencyManagement>

	<build>
		<pluginManagement>
			<plugins>
				<!-- see https://bugs.eclipse.org/bugs/show_bug.cgi?id=406507 -->
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-jar-plugin</artifactId>
					<!-- don't use 2.4 -->
					<version>2.5</version>
				</plugin>
				<!-- see https://bugs.eclipse.org/bugs/show_bug.cgi?id=406507 -->
				<plugin>
					<groupId>org.apache.maven</groupId>
					<artifactId>maven-archiver</artifactId>
					<!-- don't use 2.4 or 2.5 -->
					<version>2.6</version>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-compiler-plugin</artifactId>
					<version>3.2</version>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-source-plugin</artifactId>
					<version>2.4</version>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-javadoc-plugin</artifactId>
					<version>2.10.1</version>
				</plugin>
				<!-- AspectJ compile-time support -->
				<plugin>
					<groupId>org.codehaus.mojo</groupId>
					<artifactId>aspectj-maven-plugin</artifactId>
					<version>1.7</version>
					<!-- NB: do use 1.3 or 1.3.x due to MASPECTJ-90 - wait for 1.4 -->
					<dependencies>
						<!-- NB: You must use Maven 2.0.9 or above or these are ignored (see 
						MNG-2972) -->
						<dependency>
							<groupId>org.aspectj</groupId>
							<artifactId>aspectjrt</artifactId>
							<version>${aspectj.version}</version>
						</dependency>
						<dependency>
							<groupId>org.aspectj</groupId>
							<artifactId>aspectjtools</artifactId>
							<version>${aspectj.version}</version>
						</dependency>
					</dependencies>
					<executions>
						<execution>
							<goals>
								<goal>${aspectj.phase.1}</goal>
								<goal>${aspectj.phase.2}</goal>
							</goals>
						</execution>
					</executions>
				</plugin>
				<!-- see http://www.subshell.com/en/subshell/blog/Referencing-Maven-projects-from-Eclipse-plugins100.html -->
				<plugin>
					<groupId>org.apache.felix</groupId>
					<artifactId>maven-bundle-plugin</artifactId>
					<version>2.5.3</version>
					<extensions>true</extensions>
				</plugin>
			</plugins>
		</pluginManagement>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-jar-plugin</artifactId>
				<configuration>
					<archive>
						<index>false</index>
					</archive>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven</groupId>
				<artifactId>maven-archiver</artifactId>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<configuration>
					<source>${java.version}</source>
					<target>${java.version}</target>
					<encoding>UTF-8</encoding>
					<fork>true</fork>
          			<meminitial>128m</meminitial>
          			<maxmem>512m</maxmem>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-source-plugin</artifactId>
				<executions>
					<execution>
						<id>attach-sources</id>
						<phase>verify</phase>
						<goals>
							<goal>jar-no-fork</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-javadoc-plugin</artifactId>
				<!-- see http://maven.apache.org/plugins/maven-javadoc-plugin/aggregate-jar-mojo.html -->
				<configuration>
					<show>package</show>
					<minmemory>128m</minmemory>
					<maxmemory>1g</maxmemory>
					<!-- quiet/ -->
					<!-- verbose/ -->
					<detectLinks>false</detectLinks>
					<validateLinks>false</validateLinks>
					<!-- sourcetab>4</sourcetab -->
					<linksource>false</linksource>
					<use />
					<version />
					<!-- see http://maven.apache.org/plugins/maven-javadoc-plugin/examples/links-configuration.html -->
					<links>
						<link>http://tomcat.apache.org/tomcat-7.0-doc/servletapi</link>
						<link>http://static.springsource.org/spring/docs/3.1.x/javadoc-api</link>
						<link>http://static.springsource.org/spring-security/site/docs/3.1.x/apidocs</link>
						<link>http://asm.ow2.org/asm40/javadoc/user</link>
						<link>http://xstream.codehaus.org/javadoc</link>
						<link>http://www.eclipse.org/aspectj/doc/released/runtime-api</link>
						<link>http://www.eclipse.org/aspectj/doc/released/weaver-api</link>
						<link>http://www.extreme.indiana.edu/xmlpull-website/v1/doc/api</link>
						<link>http://commons.apache.org/lang/api-release</link>
						<link>http://commons.apache.org/collections/api-release</link>
						<link>http://commons.apache.org/io/apidocs</link>
						<link>http://commons.apache.org/pool/api-1.6</link>
						<link>http://commons.apache.org/dbcp/api-1.4</link>
						<link>http://commons.apache.org/codec/api-release</link>
						<link>http://commons.apache.org/logging/commons-logging-1.1.1/apidocs</link>
						<link>https://logging.apache.org/log4j/1.2/apidocs</link>
						<link>http://www.json.org/javadoc</link>
						<link>http://www.jdom.org/docs/apidocs</link>
						<link>http://www.slf4j.org/apidocs</link>
						<link>http://aopalliance.sourceforge.net/doc</link>
						<link>http://quartz-scheduler.org/api/1.8.5</link>
					</links>
				</configuration>
				<executions>
					<execution>
						<id>attach-javadocs</id>
						<goals>
							<goal>jar</goal>
						</goals>
						<phase>verify</phase>
					</execution>
					<execution>
						<id>aggregate-jar</id>
						<goals>
							<goal>aggregate-jar</goal>
						</goals>
						<!-- phase>site</phase -->
						<!-- phase>verify</phase -->
						<phase>verify</phase>
						<configuration>
							<linksource>false</linksource>
							<detectLinks>true</detectLinks>
							<validateLinks>true</validateLinks>
						</configuration>
					</execution>
				</executions>
			</plugin>
			<!-- AspectJ compile-time support -->
			<!-- plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>aspectj-maven-plugin</artifactId>
				<!- NB: do use 1.3 or 1.3.x due to MASPECTJ-90 - wait for 1.4 ->
				<dependencies>
					<!- NB: You must use Maven 2.0.9 or above or these are ignored (see 
					MNG-2972) ->
					<dependency>
						<groupId>org.aspectj</groupId>
						<artifactId>aspectjrt</artifactId>
						<version>${aspectj.version}</version>
					</dependency>
					<dependency>
						<groupId>org.aspectj</groupId>
						<artifactId>aspectjtools</artifactId>
						<version>${aspectj.version}</version>
					</dependency>
				</dependencies>
				<executions>
					<execution>
						<goals>
							<goal>${aspectj.phase.1}</goal>
							<goal>${aspectj.phase.2}</goal>
						</goals>
					</execution>
				</executions>
				<configuration>
					<outxml>true</outxml>
					<!- aspectLibraries>
						<aspectLibrary>
							<groupId>org.springframework</groupId>
							<artifactId>spring-aspects</artifactId>
						</aspectLibrary>
						<aspectLibrary>
							<groupId>org.springframework.security</groupId>
							<artifactId>spring-security-aspects</artifactId>
						</aspectLibrary>
					</aspectLibraries ->
					<source>${java.version}</source>
					<target>${java.version}</target>
				</configuration>
			</plugin -->
		</plugins>
	</build>

	<repositories>
		<repository>
			<id>public.maven.novabit.de</id>
			<name>Novabit Maven Repository</name>
			<url>http://maven.nuclos.de/content/groups/public</url>
			<snapshots>
				<enabled>false</enabled>
			</snapshots>
		</repository>
		<repository>
			<id>milestone.maven.springframework.org</id>
			<name>Spring External Milestone Repository</name>
			<url>http://maven.springframework.org/milestone</url>
			<snapshots>
				<enabled>false</enabled>
			</snapshots>
		</repository>
	</repositories>

	<pluginRepositories>
		<!-- 
			It seems that the aspectj-maven-plugin does not found aspectjrt if
			the repository is not repeated here. (tp)
		-->
		<pluginRepository>
			<id>central</id>
			<name>Maven Plugin Repository</name>
			<url>http://repo1.maven.org/maven2</url>
			<layout>default</layout>
		</pluginRepository>		
		<pluginRepository>
			<id>public.maven.novabit.de</id>
			<name>Novabit Maven Repository</name>
			<url>http://maven.nuclos.de/content/groups/public</url>
			<snapshots>
				<enabled>true</enabled>
				<!-- updatePolicy>always</updatePolicy -->
				<updatePolicy>interval:10</updatePolicy>
			</snapshots>
		</pluginRepository>
		<pluginRepository>
			<id>milestone.maven.springframework.org</id>
			<name>Spring External Milestone Repository</name>
			<url>http://maven.springframework.org/milestone</url>
			<snapshots>
				<enabled>false</enabled>
			</snapshots>
		</pluginRepository>
	</pluginRepositories>
  
	<distributionManagement>
		<site>
			<id>www.nuclos.de</id>
			<url>scp://www.nuclos.de/www/docs/project/</url>
		</site>
		<repository>
			<id>releases.maven.novabit.de</id>
			<url>http://maven.nuclos.de/content/repositories/releases/</url>
		</repository>
		<snapshotRepository>
			<id>snapshots.maven.novabit.de</id>
			<url>http://maven.nuclos.de/content/repositories/snapshots/</url>
		</snapshotRepository>
	</distributionManagement>

	<profiles>
		<profile>
			<id>no-ctw</id>
			<properties>
				<aspectj.phase.1>help</aspectj.phase.1>
				<aspectj.phase.2>help</aspectj.phase.2>
			</properties>
		</profile>
		<profile>
			<id>quick</id>
			<properties>
				<skipTests>true</skipTests>
				<maven.test.skip>true</maven.test.skip>
				<maven.javadoc.skip>true</maven.javadoc.skip>
				<!-- see also http://maven.apache.org/plugins/maven-source-plugin/usage.html#jar-no-fork-mojo.html -->
				<source.phase>help</source.phase>
			</properties>
		</profile>
	</profiles>

</project>
