package org.nuclos.api.parameter;

/**
 * This class represents a system parameter that has been created in Nuclos; It is usually not used
 * by rule programmers directly, but it is part of the class NuclosSystemParameter that is created during 
 * runtime containing all parameters as SystemParameter-instances.
 * 
 * @author reichama
 *
 */
public class SystemParameter {

	private String id;
	
	/**
	 * Constructor to set a new instance of the SystemParameter class
	 * using the id of a parameter that has been created in Nuclos previously
	 * 
	 * Internal use only
	 * 
	 * @param id
	 */
	public SystemParameter(String id) {
		this.id = id;
	}
	
	/**
	 * This method returns the id of the current system parameter
	 * @return
	 */
	public String getId() {
		return this.id;
	}
	
}
