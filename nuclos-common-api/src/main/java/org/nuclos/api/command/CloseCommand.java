package org.nuclos.api.command;

/**
 * Commands the client to close the current window/tab.
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class CloseCommand implements Command {
}
