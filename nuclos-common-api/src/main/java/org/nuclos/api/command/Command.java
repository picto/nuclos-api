package org.nuclos.api.command;

import java.io.Serializable;

/**
 * Represents a command the server sends to the client, e.g. to show a message, open a specific BO, etc.
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public interface Command extends Serializable {
}
