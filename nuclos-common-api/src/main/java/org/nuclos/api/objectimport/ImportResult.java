//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.objectimport;

import org.nuclos.api.common.NuclosFile;

/**
 * This class represents the return value of an import process. It is used by the ImportProvider and contains log-information
 * and messages to evaluate the success of the complete import process.
 *  
 * @author reichama
 */
public class ImportResult {

	private boolean hasErrors;
	private String  message;
	private NuclosFile logFile;
	
	public ImportResult(boolean hasErrors, String message, NuclosFile logFile) {
		this.hasErrors = hasErrors();
		this.message = message;
		this.logFile = logFile;
	}
	
	/**
	 * This method returns a boolean signifying whether the import process was successful or not.
	 * In case of errors it returns 'true'
	 * @return boolean
	 */
	public boolean hasErrors() {
		return this.hasErrors; 
	}
	
	/**
	 * This method returns all messages of the import process as a String. It may contain errors and simple debug information.
	 * Use {@link #hasErrors()} to check if the process was successful
	 * @return String
	 */
	public String getMessages() {
		return this.message;
	}
	
	/**
	 * This method returns the log file that has been created during import process
	 * @return {@link NuclosFile} logFile
	 */
	public NuclosFile getLogFile() {
		return this.logFile;
	}
	
}
