//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.communication;

/**
 * This class represents a communication port that has been configured in Nuclos; It is usually not used
 * by rule programmers directly, but it is part of a CommunicationService-System and created during 
 * runtime.
 */
public class CommunicationPortKey {
	
	private final String id;

	/**
	 * Constructor to set a new instance of the CommunicationPortKey class
	 * using the id of a communication port that has been created in Nuclos previously
	 * 
	 * Internal use only
	 * 
	 * @param id - UID of the communication port as String
	 */
	public CommunicationPortKey(String id) {
		super();
		this.id = id;
	}

	/**
	 * This method returns the id of the communication port
	 * @return
	 */
	public String getId() {
		return id;
	}
	
}
