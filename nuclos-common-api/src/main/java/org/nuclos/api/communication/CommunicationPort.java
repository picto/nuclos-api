//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.communication;

import org.nuclos.api.UID;
import org.nuclos.api.communication.response.RequestResponse;
import org.nuclos.api.context.communication.NotificationContext;
import org.nuclos.api.context.communication.RequestContext;
import org.nuclos.api.exception.BusinessException;

/**
 * 
 * A custom communication port must implement this interface. 
 * If the port receives a message or what ever, you should notify Nuclos via the
 * {@link org.nuclos.api.provider.CommunicationProvider#handleNotification(NotificationContext)}
 * 
 * {@link NotificationContext} objects are created from {@link org.nuclos.api.provider.CommunicationProvider#newContextInstance(CommunicationPort, Class)}
 * {@link NotificationContext} classes can be found in package {@link org.nuclos.api.context.communication}
 */
public interface CommunicationPort {
	
	/**
	 * 
	 * @return
	 * 		the id
	 */
	public UID getId();
	
	/**
	 * Nuclos calls this method only for the types your interface supports. 
	 * Add the supported types of {@link RequestContext} during the registration 
	 * {@link org.nuclos.api.communication.CommunicationInterface#register(org.nuclos.api.communication.CommunicationInterface.Registration)}
	 * 
	 * Reponses
	 * {@link RequestContext#clearResponse()} creates a new {@link RequestResponse}.  
	 * Send the complete context with response back to Nuclos via {@link org.nuclos.api.provider.CommunicationProvider#handleNotification(NotificationContext)}
	 * when the request is processed, in a new state...
	 * 
	 * @param context
	 * @throws BusinessException
	 */
	public <C extends RequestContext<?>> void handleRequest(C context) throws BusinessException;
	
}
