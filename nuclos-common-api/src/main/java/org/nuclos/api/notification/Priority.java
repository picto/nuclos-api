package org.nuclos.api.notification;


/**
 * The {@link Priority} class is used to specify the importance of a Nuclos notification message.<p>
 * Messages with priority LOW and NORMAL will only be displayed in the notification dialog, whereas messages with
 * priority HIGH will also cause the notification dialog to popup immediately. All messages in die notification dialog 
 * are sorted by priority.
 * 
 * @author reichama
 *
 */
public enum Priority {
	HIGH,
	NORMAL,
	LOW;
}
