//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.ws;

import org.nuclos.api.UID;

/**
 * Represents a Nuclos-WebService for calling external services as a client.<br>For rule programming 
 * the file org.nuclet.webservice.WebServices contains instances of it
 * that are passed to the {@link WebServiceProvider} for creating and initializing a client stub
 * @author Matthias Reichart
 */
public interface WebServiceObject {

	/**
	 * Returns the UID of the Nuclos-WebService. This method is usually called by the {@link WebServiceProvider}
	 * to initialize a new stub. 
	 * @return
	 */
	public UID getUID();
}
