//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.datasource;

import java.util.List;

/**
 * Represents a resultset received by DatasourceProvider after executing a
 * datasource-query
 * 
 * @author Matthias Reichart
 */
public interface DatasourceResult {
	
	/**
	 * This method returns all results of a datasource query. Each resultset is an object array that contains the content of all columns. Use getColumns()
	 * to determine which type a column is of and use it to access columns data in a type-safe way. 
	 * @return rows
	 */
	List<Object[]> getRows();
	
	
	/**
	 * This method returns a list of all columns the datasource-query contains.
	 * Every column is of type {@link DatasourceColumn} providing general column information like classtype, label and name.
	 * Use this information to cast the content of a resultset retrieved by getRows().
	 * @return columns
	 */
	List<DatasourceColumn> getColumns();
	
}
