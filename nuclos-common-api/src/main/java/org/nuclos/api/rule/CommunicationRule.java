//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.rule;

import org.nuclos.api.annotation.RuleType;
import org.nuclos.api.context.communication.CommunicationContext;
import org.nuclos.api.context.communication.PhoneCallNotificationContext;
import org.nuclos.api.exception.BusinessException;

/**
 * {@link CommunicationRule} is the interface that should be used to make an rule
 * applicable for Communication-Events. 
 * <p>Classes implementing this interface can be attached to communication ports. 
 * In case of executing a rule the method communicate is called.
 * 
 * </p>
 * @author Matthias Reichart
 */
@RuleType(name="nuclos.ruletype.communicationrule.name", description="nuclos.ruletype.communicationrule.description")
public interface CommunicationRule<C extends CommunicationContext> {

	/**
	 * This method describes the type of communication your rule is looking for. 
	 * For example, use {@link PhoneCallNotificationContext} when you want to handle phone calls only. 
	 * Use {@link CommunicationContext} if your rule should be called on all events the port sends.
	 * 
	 * @return class of at least {@link CommunicationContext}
	 */
	public Class<C> communicationContextClass();
	
	/**
	 * {@link CommunicationContext} is the context providing all custom - relevant attributes and methods.
	 * 
	 * @param context of at least {@link CommunicationContext}
	 */
	public void communicate(C context) throws BusinessException;
	
}
