//Copyright (C) 2012  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.rule;

import org.nuclos.api.annotation.RuleType;
import org.nuclos.api.context.JobContext;
import org.nuclos.api.exception.BusinessException;

/**
 * {@link JobRule} is the interface that should be used to make an rule
 * applicable for Job-Events. 
 * <p>Classes implementing this interface can be attached to jobs. 
 * In case of an Job-Event the method execute.
 * 
 * </p>
 * @author Matthias Reichart
 */
@RuleType(name="nuclos.ruletype.jobrule.name", description="nuclos.ruletype.jobrule.description")
public interface JobRule {

	/**
	 * {@link JobContext} is the context providing all attributes and methods that are relevant for Job-operations.
	 * <p>To get SessionIds the method getSessionId() can be called.
	 *
	 * @param context
	 */
	public void execute(JobContext context) throws BusinessException;
}
