//Copyright (C) 2012  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.rule;

import org.nuclos.api.annotation.RuleType;
import org.nuclos.api.context.PrintFinalContext;
import org.nuclos.api.exception.BusinessException;

/**
 * {@link PrintFinalRule} is the interface that should be used to make an rule
 * applicable for PrintFinal-Events. 
 * <p>
 * Classes implementing this interface can be attached to the printout process. 
 * </p>
 * 
 * <pre>
 * {@code
 * package de.mynuclet; 

 * import org.nuclos.api.rule.PrintFinalRule; 
 * import org.nuclos.api.context.PrintFinalContext; 
 * import org.nuclos.api.annotation.Rule; 
 * import org.nuclos.api.exception.BusinessException; 
 * import org.nuclos.api.context.PrintResult;
 * import org.nuclos.api.provider.BusinessObjectProvider;
 * 
 * @Rule(name="PrintFinalRuleBeispiel", description="PrintFinalRuleBeispiel")
 * public class PrintFinalRuleBeispiel implements PrintFinalRule {
 * 
 * 	public void printFinal(PrintFinalContext context) throws BusinessException { 
 *         final Auftrag auftrag = context.getBusinessObject(Auftrag.class);
 *         for (final PrintResult result : context.getPrintResults()) {
 *             Auftragsdokument auftragsdokument = new Auftragsdokument();
 *             auftragsdokument.setDatei(result.getOutput());
 *             auftragsdokument.setDatum(new java.util.Date());
 *             context.log("insert Auftragsdokument " + result.getOutput().getName());
 *             auftrag.insertAuftragsdokument(auftragsdokument);
 *         }
 *         BusinessObjectProvider.update(auftrag);
 * 	}
 * }
 * }
 * </pre>
 * 
 * @author Moritz Neuhaeuser <moritz.neuhaeuser@nuclos.de>
 */
@RuleType(name="nuclos.ruletype.printfinalrule.name", description="nuclos.ruletype.printfinalrule.description")
public interface PrintFinalRule {

	/**
	 * {@link org.nuclos.api.context.PrintContext} is the context providing all print - relevant attributes and methods.
	 * 
	 * @param context {@link org.nuclos.api.context.PrintContext} context
	 */
	public void printFinal(final PrintFinalContext context) throws BusinessException;
	
}
