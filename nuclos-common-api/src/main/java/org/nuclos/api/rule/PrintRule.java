//Copyright (C) 2012  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.rule;

import org.nuclos.api.annotation.RuleType;
import org.nuclos.api.context.PrintContext;
import org.nuclos.api.exception.BusinessException;

/**
 * {@link PrintRule} is the interface that should be used to make an rule
 * applicable for Print-Events. 
 * <p>
 * Classes implementing this interface can be attached to the printout process. 
 * </p>
 * <pre>
 * {@code
 * package de.mynuclet; 

 * import org.nuclos.api.rule.PrintRule;
 * import org.nuclos.api.context.PrintContext;
 * import org.nuclos.api.printout.Printout;
 * import org.nuclos.api.annotation.Rule;
 * import org.nuclos.api.exception.BusinessException;
 * import org.nuclos.api.printout.PrintoutList;
 * import de.mynuclet.formulare.*;

 * @Rule(name="PrintRuleBeispiel", description="PrintRule Beispiel")
 * public class PrintRuleBeispiel implements PrintRule {
 *      public void print(PrintContext context) throws BusinessException {
 *         final Auftrag auftrag = context.getBusinessObject(Auftrag.class);
 *         context.log("printoutlist " + context.getPrintoutList());
 *         if ("EUR".equals(auftrag.getWaehrung())) {
 *             context.log("Die Währung \"EUR\" wurde angegeben");
 * 	        for (final Printout printout : context.getPrintoutList()) {
 * 	            if (AuftragsbestaetigungLiefergeschaeftPO.class.equals(printout.getClass())) {
 * 	                context.log("attach output format " + AuftragsbestaetigungProjektgeschaeftPO.Auftrag_Projektgeschaeft.getClass().getName());
 * 	                printout.getOutputFormats().add(AuftragsbestaetigungProjektgeschaeftPO.Auftrag_Projektgeschaeft);
 * 	                context.log("after attach" + printout.getOutputFormats().size());
 * 	            } else {
 * 	                context.log(printout.getClass() + " != " + AuftragsbestaetigungLiefergeschaeftPO.class.getName());
 * 	            }
 * 	        }    
 *         }
 *     }
 * }
 * }
 * </pre>
 * @author Moritz Neuhaeuser <moritz.neuhaeuser@nuclos.de>
 */
@RuleType(name="nuclos.ruletype.printrule.name", description="nuclos.ruletype.printrule.description")
public interface PrintRule {

	/**
	 * {@link org.nuclos.api.context.PrintContext} is the context providing all print - relevant attributes and methods.
	 * 
	 * @param context {@link org.nuclos.api.context.PrintContext} context
	 */
	public void print(PrintContext context) throws BusinessException;
	
}
