package org.nuclos.api.locale;

/**
 * NuclosLocale contains all locales Nuclos works with.<br>
 * <br> 
 * Elements of this class are usually used in rule programming, <br>e.g. 
 * when executing reports (via {@link ReportProvider}) or printouts (via {@link PrintoutProvider}) or when reading or setting values of 
 * localized fields. <br>
 * 
 * @author reichama
 *
 */
public enum NuclosLocale {	
	
	/** Language: Malay,
	 * Country: Malaysia **/
	MS_MY("ms","MY"), 
	/** Language: Arabic,
	 * Country: Qatar **/
	AR_QA("ar","QA"), 
	/** Language: Icelandic,
	 * Country: Iceland **/
	IS_IS("is","IS"), 
	/** Language: Finnish,
	 * Country: Finland **/
	FI_FI("fi","FI"), 
	/** Language: English,
	 * Country: Malta **/
	EN_MT("en","MT"), 
	/** Language: Italian,
	 * Country: Switzerland **/
	IT_CH("it","CH"), 
	/** Language: Dutch,
	 * Country: Belgium **/
	NL_BE("nl","BE"), 
	/** Language: Arabic,
	 * Country: Saudi Arabia **/
	AR_SA("ar","SA"), 
	/** Language: Arabic,
	 * Country: Iraq **/
	AR_IQ("ar","IQ"), 
	/** Language: Spanish,
	 * Country: Puerto Rico **/
	ES_PR("es","PR"), 
	/** Language: Spanish,
	 * Country: Chile **/
	ES_CL("es","CL"), 
	/** Language: German,
	 * Country: Austria **/
	DE_AT("de","AT"), 
	/** Language: English,
	 * Country: United Kingdom **/
	EN_GB("en","GB"), 
	/** Language: Spanish,
	 * Country: Panama **/
	ES_PA("es","PA"), 
	/** Language: Arabic,
	 * Country: Yemen **/
	AR_YE("ar","YE"), 
	/** Language: Macedonian,
	 * Country: Macedonia **/
	MK_MK("mk","MK"), 
	/** Language: English,
	 * Country: Canada **/
	EN_CA("en","CA"), 
	/** Language: Vietnamese,
	 * Country: Vietnam **/
	VI_VN("vi","VN"), 
	/** Language: Dutch,
	 * Country: Netherlands **/
	NL_NL("nl","NL"), 
	/** Language: Spanish,
	 * Country: United States **/
	ES_US("es","US"), 
	/** Language: Chinese,
	 * Country: China **/
	ZH_CN("zh","CN"), 
	/** Language: Spanish,
	 * Country: Honduras **/
	ES_HN("es","HN"), 
	/** Language: English,
	 * Country: United States **/
	EN_US("en","US"), 
	/** Language: Arabic,
	 * Country: Morocco **/
	AR_MA("ar","MA"), 
	/** Language: Indonesian,
	 * Country: Indonesia **/
	IN_ID("in","ID"), 
	/** Language: English,
	 * Country: South Africa **/
	EN_ZA("en","ZA"), 
	/** Language: Korean,
	 * Country: South Korea **/
	KO_KR("ko","KR"), 
	/** Language: Arabic,
	 * Country: Tunisia **/
	AR_TN("ar","TN"), 
	/** Language: Serbian,
	 * Country: Serbia **/
	SR_RS("sr","RS"), 
	/** Language: Belarusian,
	 * Country: Belarus **/
	BE_BY("be","BY"), 
	/** Language: Chinese,
	 * Country: Taiwan **/
	ZH_TW("zh","TW"), 
	/** Language: Arabic,
	 * Country: Sudan **/
	AR_SD("ar","SD"), 
	/** Language: Japanese,
	 * Country: Japan **/
	JA_JP("ja","JP"), 
	/** Language: Spanish,
	 * Country: Bolivia **/
	ES_BO("es","BO"), 
	/** Language: Arabic,
	 * Country: Algeria **/
	AR_DZ("ar","DZ"), 
	/** Language: Spanish,
	 * Country: Argentina **/
	ES_AR("es","AR"), 
	/** Language: Arabic,
	 * Country: United Arab Emirates **/
	AR_AE("ar","AE"), 
	/** Language: French,
	 * Country: Canada **/
	FR_CA("fr","CA"), 
	/** Language: Lithuanian,
	 * Country: Lithuania **/
	LT_LT("lt","LT"), 
	/** Language: Serbian,
	 * Country: Montenegro **/
	SR_ME("sr","ME"), 
	/** Language: Arabic,
	 * Country: Syria **/
	AR_SY("ar","SY"), 
	/** Language: Russian,
	 * Country: Russia **/
	RU_RU("ru","RU"), 
	/** Language: French,
	 * Country: Belgium **/
	FR_BE("fr","BE"), 
	/** Language: Spanish,
	 * Country: Spain **/
	ES_ES("es","ES"), 
	/** Language: Hebrew,
	 * Country: Israel **/
	IW_IL("iw","IL"), 
	/** Language: Danish,
	 * Country: Denmark **/
	DA_DK("da","DK"), 
	/** Language: Spanish,
	 * Country: Costa Rica **/
	ES_CR("es","CR"), 
	/** Language: Chinese,
	 * Country: Hong Kong **/
	ZH_HK("zh","HK"), 
	/** Language: Catalan,
	 * Country: Spain **/
	CA_ES("ca","ES"), 
	/** Language: Thai,
	 * Country: Thailand **/
	TH_TH("th","TH"), 
	/** Language: Ukrainian,
	 * Country: Ukraine **/
	UK_UA("uk","UA"), 
	/** Language: Spanish,
	 * Country: Dominican Republic **/
	ES_DO("es","DO"), 
	/** Language: Spanish,
	 * Country: Venezuela **/
	ES_VE("es","VE"), 
	/** Language: Polish,
	 * Country: Poland **/
	PL_PL("pl","PL"), 
	/** Language: Arabic,
	 * Country: Libya **/
	AR_LY("ar","LY"), 
	/** Language: Arabic,
	 * Country: Jordan **/
	AR_JO("ar","JO"), 
	/** Language: Hungarian,
	 * Country: Hungary **/
	HU_HU("hu","HU"), 
	/** Language: Spanish,
	 * Country: Guatemala **/
	ES_GT("es","GT"), 
	/** Language: Spanish,
	 * Country: Paraguay **/
	ES_PY("es","PY"), 
	/** Language: Bulgarian,
	 * Country: Bulgaria **/
	BG_BG("bg","BG"), 
	/** Language: Croatian,
	 * Country: Croatia **/
	HR_HR("hr","HR"), 
	/** Language: Romanian,
	 * Country: Romania **/
	RO_RO("ro","RO"), 
	/** Language: French,
	 * Country: Luxembourg **/
	FR_LU("fr","LU"), 
	/** Language: English,
	 * Country: Singapore **/
	EN_SG("en","SG"), 
	/** Language: Spanish,
	 * Country: Ecuador **/
	ES_EC("es","EC"), 
	/** Language: Spanish,
	 * Country: Nicaragua **/
	ES_NI("es","NI"), 
	/** Language: Spanish,
	 * Country: El Salvador **/
	ES_SV("es","SV"), 
	/** Language: Hindi,
	 * Country: India **/
	HI_IN("hi","IN"), 
	/** Language: Greek,
	 * Country: Greece **/
	EL_GR("el","GR"), 
	/** Language: Slovenian,
	 * Country: Slovenia **/
	SL_SI("sl","SI"), 
	/** Language: Italian,
	 * Country: Italy **/
	IT_IT("it","IT"), 
	/** Language: German,
	 * Country: Luxembourg **/
	DE_LU("de","LU"), 
	/** Language: French,
	 * Country: Switzerland **/
	FR_CH("fr","CH"), 
	/** Language: Maltese,
	 * Country: Malta **/
	MT_MT("mt","MT"), 
	/** Language: Arabic,
	 * Country: Bahrain **/
	AR_BH("ar","BH"),
	/** Language: Portuguese,
	 * Country: Brazil **/
	PT_BR("pt","BR"), 
	/** Language: Norwegian,
	 * Country: Norway **/
	NO_NO("no","NO"), 
	/** Language: German,
	 * Country: Switzerland **/
	DE_CH("de","CH"), 
	/** Language: Chinese,
	 * Country: Singapore **/
	ZH_SG("zh","SG"), 
	/** Language: Arabic,
	 * Country: Kuwait **/
	AR_KW("ar","KW"), 
	/** Language: Arabic,
	 * Country: Egypt **/
	AR_EG("ar","EG"), 
	/** Language: Irish,
	 * Country: Ireland **/
	GA_IE("ga","IE"), 
	/** Language: Spanish,
	 * Country: Peru **/
	ES_PE("es","PE"), 
	/** Language: Czech,
	 * Country: Czech Republic **/
	CS_CZ("cs","CZ"), 
	/** Language: Turkish,
	 * Country: Turkey **/
	TR_TR("tr","TR"), 
	/** Language: Spanish,
	 * Country: Uruguay **/
	ES_UY("es","UY"), 
	/** Language: English,
	 * Country: Ireland **/
	EN_IE("en","IE"), 
	/** Language: English,
	 * Country: India **/
	EN_IN("en","IN"), 
	/** Language: Arabic,
	 * Country: Oman **/
	AR_OM("ar","OM"), 
	/** Language: Serbian,
	 * Country: Serbia and Montenegro **/
	SR_CS("sr","CS"), 
	/** Language: Albanian,
	 * Country: Albania **/
	SQ_AL("sq","AL"), 
	/** Language: Portuguese,
	 * Country: Portugal **/
	PT_PT("pt","PT"), 
	/** Language: Latvian,
	 * Country: Latvia **/
	LV_LV("lv","LV"), 
	/** Language: Slovak,
	 * Country: Slovakia **/
	SK_SK("sk","SK"), 
	/** Language: Spanish,
	 * Country: Mexico **/
	ES_MX("es","MX"), 
	/** Language: English,
	 * Country: Australia **/
	EN_AU("en","AU"), 
	/** Language: English,
	 * Country: New Zealand **/
	EN_NZ("en","NZ"), 
	/** Language: Swedish,
	 * Country: Sweden **/
	SV_SE("sv","SE"), 
	/** Language: Arabic,
	 * Country: Lebanon **/
	AR_LB("ar","LB"), 
	/** Language: German,
	 * Country: Germany **/
	DE_DE("de","DE"), 
	/** Language: Spanish,
	 * Country: Colombia **/
	ES_CO("es","CO"), 
	/** Language: English,
	 * Country: Philippines **/
	EN_PH("en","PH"), 
	/** Language: Estonian,
	 * Country: Estonia **/
	ET_EE("et","EE"), 
	/** Language: Greek,
	 * Country: Cyprus **/
	EL_CY("el","CY"), 
	/** Language: French,
	 * Country: France **/
	FR_FR("fr","FR"); 

	private String language;
	private String country;
	
	private NuclosLocale(String language, String country) {
		this.language = language;
		this.country = country;
	}

	@Override
	public String toString() {
		return language + "_" + country;
	}
	
}
