package org.nuclos.api.statemodel;

import org.nuclos.api.UID;


/**
 * Represents a state of a nuclos statemodel. This class is used in rule programming 
 * to execute a state transition via rg.nuclos.api.provider.StatemodelProvider .
 * 
 * <p> 
 * @author Matthias Reichart
 */
public class State {
	
	private UID uid;
	private String description;
	private String name;
	private Integer numeral;
	private UID model;
	
	public State(UID uid, String description, String name, Integer numeral, UID model) {
		this.uid = uid;
		this.description = description;
		this.name = name;
		this.numeral = numeral;
		this.model = model;
	}

	
	/**
	 * This method returns the Id of the current state object
	 * @return
	 */
	public UID getId() {
		return this.uid;
	}

	/**
	 * This method returns the description of the current state object
	 * @return
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 * This method returns the state numeral of the current state object
	 * @return
	 */
	public Integer getNumeral() {
		return this.numeral;
	}

	/**
	 * This method returns the name of the current state object
	 * @return
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * This method returns the id of the statemodel the current state object belongs to
	 * @return
	 */
	public UID getModelId() {
		return this.model;
	}
	
}
