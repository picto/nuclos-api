//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.context;

import java.util.List;

/**
 * Context for scripting expressions.
 * Retrieve context data by an expression language.
 * See the following examples
 * <ul>
 * 	<li><code>#{[namespace].[entity]}</code> returns a {@link List} of related contexts (i.e. subforms).</li>
 * 	<li><code>#{[namespace].[entity].[field]}</code> returns the value of a field.</li>
 *  <li><code>#{[namespace].[entity].[field].value}</code> is equal to <code>#{[namespace].[entity].[field]}</code>.</li>
 *  <li><code>#{[namespace].[entity].[field].id}</code> returns the id of a reference field as {@link Long}.</li>
 *  <li><code>#{[namespace].[entity].[field].context}</code> returns the context of a referenced object as {@link ScriptContext}.</li>
 * </ul>
 *
 * @author	thomas.schiffmann
 */
public interface ScriptContext {

	public static final Object CANCEL = new Object();

	String getUsername();

	Object propertyMissing(String name);

	void propertyMissing(String name, Object value);

	Object methodMissing(String name, Object args);
}
