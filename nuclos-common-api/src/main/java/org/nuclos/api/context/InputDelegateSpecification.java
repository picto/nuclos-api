//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.context;

import java.io.Serializable;
import java.util.Map;

/**
 * Specification of a client-side delegate class to produce a customized input panel.
 * It is possible to pass a map of serializable values to the delegate, for instance to setup comboboxes with context-dependant values.
 *
 * @author	thomas.schiffmann
 */
@SuppressWarnings("serial")
public class InputDelegateSpecification implements Serializable {
	
	/**
	 * The fully qualified class name of the delegate class.
	 * org.nuclos.api.context.InputDelegate
	 */
	private final String delegateclass;
	
	/**
	 * The data provided to the delegate instance.
	 */
	private Map<String, Serializable> data;
	
	private boolean applyForAllMultiEditObjects = false;

	/**
	 * Constructs a new InputDelegateSpecification.
	 * @param delegateclass The fully qualified name of the delegate class.
	 */
	public InputDelegateSpecification(String delegateclass) {
		super();
		this.delegateclass = delegateclass;
	}

	/**
	 * Getter for delegate class name.
	 * @return The fully qualified name of the delegate class.
	 */
	public String getDelegateClass() {
		return delegateclass;
	}
	
	/**
	 * Getter for context data to be passed to delegate instance.
	 * @return Context data to be passed to delegate instance.
	 */
	public Map<String, Serializable> getData() {
		return data;
	}

	/**
	 * Setter for context data to be passed to delegate instance.
	 * @param data Context data to be passed to delegate instance.
	 */
	public void setData(Map<String, Serializable> data) {
		this.data = data;
	}
	
	/**
	 * internal use only
	 * @return applyForAllMultiEditObjects
	 */
	public boolean applyForAllMultiEditObjects() {
		return applyForAllMultiEditObjects;
	}

	/**
	 * @param applyForAllMultiEditObjects
	 * 		false (default): One input question for every object during a multi edit.		
	 * 		true: The client is instructed to ask for the input only once. 
	 * 			You have to choose a general key for your input to work as expected, no object specific data within the key.
	 */
	public void setApplyForAllMultiEditObjects(boolean applyForAllMultiEditObjects) {
		this.applyForAllMultiEditObjects = applyForAllMultiEditObjects;
	}
}
