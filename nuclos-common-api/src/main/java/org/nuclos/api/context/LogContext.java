//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.context;



/**
 * {@link LogContext} represents a context with basic logging functionality.
 *
 */
public interface LogContext {

	/**
	 * This method uses log4j to log an info message into the log-files of the server
	 * 
	 * @param message
	 */
	public void log(String message);
	
	/**
	 * This method uses log4j to log a warning message into the log-files of the server
	 * 
	 * @param message
	 */
	public void logWarn(String message);

	/**
	 * This method uses log4j to log an error into the log-files of the server. For this a message text
	 * and an exception can be passed as arguments
	 * 
	 * @param message
	 * @param ex
	 */
	public void logError(String message, Exception ex);
	
}
