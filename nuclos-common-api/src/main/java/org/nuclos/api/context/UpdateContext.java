//Copyright (C) 2012  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.context;

import org.nuclos.api.businessobject.BusinessObject;
import org.nuclos.api.businessobject.GenericBusinessObject;
import org.nuclos.api.businessobject.facade.Modifiable;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.notification.Priority;


/**
 * {@link UpdateContext} represents the context used in an Update-Rule, Update and UpdateFinal
 * <p>It contains the {@link BusinessObject} that has been changed at runtime and several functions like caching
 *
 * @see RuleContext
 * @author Matthias Reichart
 */
public interface UpdateContext extends RuleContext {
	

	/**
	 * This method returns the {@link BusinessObject} containing the entry-data that has been changed
	 * during runtime. The {@link BusinessObject} must be a Modifiable
	 * 
	 * @param t Classtype being a {@link Modifiable}
	 * @return {@link BusinessObject} extending {@link Modifiable}
	 */
	public <T extends Modifiable> T getBusinessObject(Class<T> t);

	/**
	 * This method returns the {@link GenericBusinessObject} containing the entry-data that has been changed
	 * during runtime.
	 *
	 * @param t Classtype being a {@link GenericBusinessObject}
	 * @return {@link GenericBusinessObject} extending {@link GenericBusinessObject}
	 */
	public <T extends GenericBusinessObject> T getGenericBusinessObject(Class<T> t) throws BusinessException;
	
	/**
	 * Returns true if update is called from a collective process ("Sammelbearbeitung").
	 * @return true if update is called from a collective process ("Sammelbearbeitung")
	 */
	public boolean isCollectiveProcessing();
	
	/**
	 * Creates a message with a {@link Priority}. The message is displayed
	 * in the notification dialog in nuclos. Please check class Priority to get more
	 * information about the priority-handling.
	 * @param message
	 * @param prio
	 */
	public void notify(String message, Priority prio);
	
	/**
	 * Creates a message with a {@link Priority}. The message is displayed
	 * in the notification dialog in nuclos as a message with priority 'normal'. Please check class Priority to get more
	 * information about the priority-handling.
	 * @param message
	 */
	public void notify(String message);
}
