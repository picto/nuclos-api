//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.context;

import java.io.Serializable;

/**
 * Specification of input dialog.
 *
 * @author	thomas.schiffmann
 */
public class InputSpecification implements Serializable{

	private static final long serialVersionUID = 5844431578353108711L;

	/**
	 * Input type to show a yes/no dialog.
	 */
	public static final int CONFIRM_YES_NO = 0;

	/**
	 * Input type to show a ok/cancel dialog. If the user choosed cancel, the request won't execute again.
	 */
	public static final int CONFIRM_OK_CANCEL = 1;

	/**
	 * Variable value for ok.
	 */
	public static final int OK = 0;

	/**
	 * Variable value for yes.
	 */
	public static final int YES = 1;

	/**
	 * Variable value for no.
	 */
	public static final int NO = 2;

	/**
	 * Input type to show string input dialog.
	 */
	public static final int INPUT_VALUE = 10;

	/**
	 * Input type to show a select option dialog.
	 */
	public static final int INPUT_OPTION = 20;

	private final int type;
	private final String key;
	private final String message;

	private Object[] options;
	private Object defaultOption;
	
	private boolean applyForAllMultiEditObjects = false;

	/**
	 * Create a new {@link InputSpecification}
	 * @param type Input type (CONFIRM_YES_NO, CONFIRM_OK_CANCEL, INPUT_VALUE or INPUT_OPTION)
	 * @param key Variable key (to obtain the value)
	 * @param message Dialog message
	 */
	public InputSpecification(int type, String key, String message) {
		super();
		this.type = type;
		this.key = key;
		this.message = message;
	}

	/**
	 * Get input type.
	 * @return Input type
	 */
	public int getType() {
		return type;
	}

	/**
	 * Get the variable key/name.
	 * @return Variable key/name
	 */
	public String getKey() {
		return key;
	}

	/**
	 * Get the dialog message.
	 * @return Dialog message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * Get available input options, only applicable for INPUT_OPTION.
	 * @return Input options
	 */
	public Object[] getOptions() {
		return options;
	}

	/**
	 * Set available input options, only applicable for INPUT_OPTION.
	 * @param options Available input options
	 */
	public void setOptions(Object[] options) {
		this.options = options;
	}

	/**
	 * Get default option (of available options, only applicable for INPUT_OPTION)
	 * @return Default option.
	 */
	public Object getDefaultOption() {
		return defaultOption;
	}

	/**
	 * Set default option (of available options, only applicable for INPUT_OPTION)
	 * @param defaultOption Default option
	 */
	public void setDefaultOption(Object defaultOption) {
		this.defaultOption = defaultOption;
	}

	/**
	 * internal use only
	 * @return applyForAllMultiEditObjects
	 */
	public boolean applyForAllMultiEditObjects() {
		return applyForAllMultiEditObjects;
	}

	/**
	 * @param applyForAllMultiEditObjects
	 * 		false (default): One input question for every object during a multi edit.		
	 * 		true: The client is instructed to ask for the input only once. 
	 * 			You have to choose a general key for your input to work as expected, no object specific data within the key.
	 */
	public void setApplyForAllMultiEditObjects(boolean applyForAllMultiEditObjects) {
		this.applyForAllMultiEditObjects = applyForAllMultiEditObjects;
	}
	
	
}
