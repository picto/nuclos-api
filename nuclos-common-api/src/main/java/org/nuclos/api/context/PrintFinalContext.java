//Copyright (C) 2012  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.context;

import java.util.Collection;

import org.nuclos.api.businessobject.BusinessObject;
import org.nuclos.api.businessobject.GenericBusinessObject;
import org.nuclos.api.common.NuclosFile;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.rule.PrintFinalRule;

/**
 * {@link PrintFinalContext} represents the context used in {@link PrintFinalRule}
 * <p>
 * It provides a {@link PrintResult} of the execution context
 * </p>
 * @see RuleContext
 * @author Moritz Neuhaeuser<moritz.neuhaeuser@nuclos.de>
 */
public interface PrintFinalContext extends RuleContext {

	/**
	 * returns collection of {@link PrintResult}
	 * 
	 * @return {@link NuclosFile}
	 */
	public Collection<PrintResult> getPrintResults();
	
	/**
	 * returns {@link BusinessObject}
	 * 
	 * is not persisted automatically
	 * 
	 * @return  {@link BusinessObject}
	 */
	public <T extends BusinessObject> T getBusinessObject(Class<T> t);

	/**
	 * returns {@link GenericBusinessObject}
	 *
	 * is not persisted automatically
	 *
	 * @param t Classtype being a {@link GenericBusinessObject}
	 * @return {@link GenericBusinessObject} extending {@link GenericBusinessObject}
	 */
	public <T extends GenericBusinessObject> T getGenericBusinessObject(Class<T> t) throws BusinessException;


}
