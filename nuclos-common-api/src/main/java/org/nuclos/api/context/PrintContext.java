//Copyright (C) 2012  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.context;

import org.nuclos.api.businessobject.BusinessObject;
import org.nuclos.api.businessobject.GenericBusinessObject;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.printout.Printout;
import org.nuclos.api.printout.PrintoutList;
import org.nuclos.api.rule.PrintRule;

/**
 * {@link PrintContext} represents the context used in {@link PrintRule}
 * <p>
 * It provides a {@link PrintoutList} which is applied to the
 * {@link BusinessObject} in the execution context
 * </p>
 * @see RuleContext
 * @author Moritz Neuhaeuser<moritz.neuhaeuser@nuclos.de>
 */
public interface PrintContext extends RuleContext {

	/**
	 * returns {@link PrintoutList} for the {@link BusinessObject}
	 * in the execution context
	 * 
	 * @return {@link Printout} 
	 */
	public PrintoutList getPrintoutList();
	
	/**
	 * returns {@link BusinessObject}
	 * 
	 * is not persisted automatically
	 * 
	 * @return  {@link BusinessObject}
	 */
	public <T extends BusinessObject> T getBusinessObject(Class<T> t);

	/**
	 * returns {@link GenericBusinessObject}
	 *
	 * is not persisted automatically
	 *
	 * @param t Classtype being a {@link GenericBusinessObject}
	 * @return {@link GenericBusinessObject} extending {@link GenericBusinessObject}
	 */
	public <T extends GenericBusinessObject> T getGenericBusinessObject(Class<T> t) throws BusinessException;

}
