//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.context.communication;

import org.nuclos.api.UID;

public interface PhoneCallNotificationContext extends NotificationContext, InstantiableContext {
	
	public static enum State {
		RINGING,
		START,
		END
	}
		
	/**
	 * 
	 * @param state
	 */
	public void setState(State state);
	
	/**
	 * @param fromNumber
	 * 			calling phone number,
	 * 			(could be null)
	 */
	public void setFromNumber(String fromNumber);
	
	/**
	 * @param user
	 * 			if your interface could identify the calling Nuclos user.
	 * 			(could be null)
	 */
	public void setFromUserId(UID fromUserId);

	/**
	 * @param serviceNumber
	 * 			the called service number
	 * 			(could be null)
	 */
	public void setServiceNumber(String serviceNumber);
	
	/**
	 * 
	 * @param toNumber
	 * 			the called phone number, or in case of a service number,
	 * 			the number of the service team member 
	 */
	public void setToNumber(String toNumber);
	
	/**
	 * @param user
	 * 			if your interface could identify the called Nuclos user.
	 * 			(could be null)
	 */
	public void setToUserId(UID toUserId);

	/**
	 * 
	 * @param id
	 */
	public void setId(String id);
	
	/**
	 * 
	 * @return
	 */
	public State getState();

	/**
	 * (could be null)
	 * @return
	 */
	public String getFromNumber();
	
	/**
	 * (could be null)
	 * @return
	 */
	public UID getFromUserId();

	/**
	 * (could be null)
	 * @return
	 */
	public String getServiceNumber();
	
	/**
	 * 
	 * @return
	 */
	public String getToNumber();

	/**
	 * (could be null)
	 * @return
	 */
	public UID getToUserId();
	
	/**
	 * 
	 * @return
	 */
	public String getId();
	
}
