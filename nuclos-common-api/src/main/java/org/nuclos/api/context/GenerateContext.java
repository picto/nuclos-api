//Copyright (C) 2012  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.context;

import java.util.Collection;

import org.nuclos.api.businessobject.BusinessObject;
import org.nuclos.api.businessobject.GenericBusinessObject;
import org.nuclos.api.businessobject.facade.Modifiable;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.notification.Priority;

/**
 * {@link GenerateContext} represents the context used in Generation-Rule
 * <p>It contains the TargetObjects and the SourceObjects and several functions like caching
 *
 * @see RuleContext
 * @author Matthias Reichart
 */
public interface GenerateContext extends RuleContext {

	/**
	 * This method returns a modifiable {@link BusinessObject} that represents the source; if there is more than
	 * one SourceObject a BusinessException will be thrown; In this case use getSourceObjects(Class&lt;T&gt; t) 
	 * instead
	 */
	public <T extends Modifiable> T getSourceObject(Class<T> t) throws BusinessException;
	
	/**
	 * This method returns a list of modifiable {@link BusinessObject} that represent the Sources
	 */
	public <T extends Modifiable> Collection<T> getSourceObjects(Class<T> t);
	
	/**
	 * This method returns a modifiable {@link BusinessObject} that represent the Target
	 */
	public <T extends Modifiable> T getTargetObject(Class<T> t);

	/**
	 * This method returns the ParameterObject
	 */
	public BusinessObject getParameterObject();

	/**
	 * This method returns the {@link GenericBusinessObject} that represents the source; if there is more than
	 * one SourceObject a BusinessException will be thrown; In this case use getSourceObjects(Class&lt;T&gt; t)
	 * instead
	 */
	public <T extends GenericBusinessObject> T getSourceGenericObject(Class<T> t) throws BusinessException;

	/**
	 * This method returns a list of {@link GenericBusinessObject}s that represent the Sources

	 */
	public <T extends GenericBusinessObject> Collection<T> getSourceGenericObjects(Class<T> t) throws BusinessException;

	/**
	 * This method returns a {@link GenericBusinessObject} that represent the Target
	 */
	public <T extends GenericBusinessObject> T getTargetGenericObject(Class<T> t) throws BusinessException;

	/**
	 * This method returns a {@link GenericBusinessObject} that represent the Parameter
	 */
	public <T extends GenericBusinessObject> T getParameterGenericObject(Class<T> t) throws BusinessException;
	
	/**
	 * Creates a message with a {@link Priority}. The message is displayed
	 * in the notification dialog in nuclos. The target of the current generation is 
	 * stored inside the context. The source must be set by the user.
	 * Please check class Priority to get more information about the priority-handling.
	 */
	public void notify(String message, Priority prio, BusinessObject source);
	
	/**
	 * Creates a message with a {@link Priority}. The message is displayed
	 * in the notification dialog in nuclos as a message with priority 'normal'. The target of the current generation is 
	 * stored inside the context. The source must be set by the user.
	 */
	public void notify(String message, BusinessObject source);
	
}
