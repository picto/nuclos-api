//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.context;

import java.io.Serializable;

/**
 * {@link InputContext} adds support for requesting input from the user.
 * To check if the creator of the request that is executed is able to provide 
 * additional information, use <code>isSupported()</code>.
 * <p>
 * There is only one instance of this class (i.e. a singleton). Compared to 
 * all-static methods, this works better in a Spring-driven environment.
 * </p>
 * @see 	InputSpecification
 * @see		InputRequiredException
 * @author	thomas.schiffmann
 */
// @Configurable
public class InputContext {
	
	private final static InputContext INSTANCE = new InputContext();
	
	private InputContext() {
	}

	public static InputContext getInstance() {
		return INSTANCE;
	}
	
	/**
	 * Get value of requested variable
	 * @param key Requested variable
	 * @return Value of requested variable, null if not answered.
	 */
	public static Serializable get(String key) {
		return SpringInputContext.getInstance().get(key);
	}

	/**
	 * Is the creator of the request able to provide additional input?
	 * @return True, if request creator can handle {@link InputRequiredException}s, otherwise false.
	 */
	public static boolean isSupported() {
		return SpringInputContext.getInstance().isSupported();
	}
}
