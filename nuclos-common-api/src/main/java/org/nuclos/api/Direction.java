//Copyright (C) 2012  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api;

import java.io.Serializable;

/**
 * TODO: Javadoc
 * TODO: Move this class to org.nuclos.api.command (see NUCLOS-5221)
 */
public interface Direction<PK> extends Serializable {

	public enum AdditionalNavigation implements Serializable {
		LOGIN, LOGOUT
	}

	/**
	 * Returns the primary key of this object
	 */
	PK getId();

	/**
	 * Returns the UID of the entity the BusinesObject bases on
	 */
	UID getEntityUid();

	/**
	 * Returns the name of the entity the BusinesObject bases on
	 */
	String getEntity();

	/**
	 * Returns is it to publish in new client tab / browser tab
	 */
	boolean isNewTab();

	/**
	 * Returns navigation options where not based on entity
	 */
	AdditionalNavigation getAdditionalNavigation();

}
