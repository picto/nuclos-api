//Copyright (C) 2012  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.businessobject;

import org.nuclos.api.UID;


/**
 * {@link BusinessObject} represents an entry of a configured entity. Its methods correspond
 * to the attributes of the entity.
 * 
 * <p> 
 * @author Matthias Reichart
 */
public interface BusinessObject<PK> {

	/**
	 * Returns the primary key of this object
	 */
	PK getId();

	/**
	 * Returns the UID of the entity the BusinesObject bases on
	 */
	UID getEntityUid();
	
	/**
	 * Returns the name of the entity the BusinesObject bases on
	 */
	String getEntity();
	
	/**
	 * Returns the current version of this object
	 */
	Integer getVersion();

	/**
	 * Is this object a new one?
	 */
	boolean isInsert();

	/**
	 * Object is modified?
	 */
	boolean isUpdate();

	/**
	 * Object is removed?
	 */
	boolean isDelete();
	
}
