package org.nuclos.api.businessobject.facade;

import org.nuclos.api.UID;
import org.nuclos.api.businessobject.BusinessObject;
import org.nuclos.api.exception.BusinessException;

/**
 * Lockable is the interface a {@link BusinessObject} must implement to be used for locking.
 * If an owner is set, the business object is locked by him or her. Unlock will remove an owner.
 * 
 * @since Nuclos 4.10
 */
public interface Lockable<PK> extends BusinessObject<PK> {

	/**
	 * Lock this business object by setting the current user as owner.
	 * Uses a new transaction but will not increment the version!
	 * 
	 * @throws BusinessException
	 */
	public void lock() throws BusinessException;
	
	/**
	 * Lock this business object by setting the given owner.
	 * Uses a new transaction but will not increment the version!
	 * 
	 * @param ownerId
	 * @throws BusinessException
	 */
	public void lock(UID ownerId) throws BusinessException;
	
	/**
	 * Unlock this business object by removing the owner. 
	 * Uses a new transaction but will not increment the version!
	 * 
	 * @throws BusinessException
	 */
	public void unlock() throws BusinessException;
	
	/**
	 * Unlock this business object after commit by removing the owner. 
	 * Uses a new transaction but will not increment the version!
	 * 
	 * @throws BusinessException
	 */
	public void unlockAfterCommit() throws BusinessException;
	
}
