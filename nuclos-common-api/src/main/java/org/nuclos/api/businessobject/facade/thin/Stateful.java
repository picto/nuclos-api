package org.nuclos.api.businessobject.facade.thin;

import org.nuclos.api.UID;
import org.nuclos.api.businessobject.BusinessObject;
import org.nuclos.api.rule.StateChangeFinalRule;
import org.nuclos.api.rule.StateChangeRule;

/**
 * Stateful is the interface a {@link BusinessObject} must implement to be 
 * used in {@link StateChangeRule} and {@link StateChangeFinalRule} rules
 * 
 * @since Nuclos 4.11
 * 		Use the new 'thin' property for your business object to improve the performance!
 */
public interface Stateful {

	/**
	 * Returns the id of the current state
	 * @return
	 */
	UID getNuclosStateId();
	
}
