//Copyright (C) 2012  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.businessobject;

import java.util.ArrayList;
import java.util.List;

import org.nuclos.api.businessobject.attribute.Attribute;

/**
 * {@link SearchExpression} represents a search expression for a {@link Query}. It enabled the user
 * to join other SearchExpressions and specify the query parameter that are used to retrieve results
 * 
 * <p> 
 * @author Matthias Reichart
 */
public class SearchExpression<T> {

	Attribute<T> source;
	Attribute<T> target;
	T value;
	QueryOperation operator;
	
	QueryOperation parentOperator;
	SearchExpression<T> parentSearchExpression;
	
	List<SearchExpressionPair<T>> childSearchExpression;
	
	/**
	 * This Constructor is used when there are two attributes to compare
	 * 
	 * @param pSource
	 * @param pTarget
	 * @param pOperator
	 */
	public SearchExpression(Attribute<T> pSource, 
										  Attribute<T> pTarget, QueryOperation pOperator) {
		this.source = pSource;
		this.target = pTarget;
		this.operator = pOperator;
		this.childSearchExpression = new ArrayList<SearchExpressionPair<T>>();
	}
	
	/**
	 * This Constructor is used when a attributes is compared to a given value with a given operator
	 * 
	 * @param pSource
	 * @param pValue
	 * @param pOperator
	 */
	public SearchExpression(Attribute<T> pSource, T pValue, QueryOperation pOperator) {

		this.source = pSource;
		this.value = pValue;
		this.operator = pOperator;
		this.childSearchExpression = new ArrayList<SearchExpressionPair<T>>();
	}
	
	/**
	 * This Constructor is used when a attributes is checkd on Null or notNull
	 * 
	 * @param pSource
	 * @param pOperator
	 */
	public SearchExpression(Attribute<T> pSource, QueryOperation pOperator) {

		this.source = pSource;
		this.value = null;
		this.operator = pOperator;
		this.childSearchExpression = new ArrayList<SearchExpressionPair<T>>();
	}
	
	public SearchExpression(SearchExpression<T> pParentSearchExpression, QueryOperation pOperator) {

		this.parentSearchExpression = pParentSearchExpression;
		this.operator = pOperator;
		this.childSearchExpression = new ArrayList<SearchExpressionPair<T>>();
	}
	
	/**
	 * This Constructor is used to join the current SearchExpression with a second SearchExpression using AND
	 */
	public SearchExpression and(SearchExpression pSecondSearchExpression) {
		this.childSearchExpression.add( 
				new SearchExpressionPair<T>(QueryOperation.AND, pSecondSearchExpression));
		return this;
	}
	
	/**
	 * This Constructor is used to join the current SearchExpression with a second SearchExpression using OR
	 * 
	 * @param pSecondSearchExpression
	 */
	public SearchExpression or(SearchExpression pSecondSearchExpression) {
		this.childSearchExpression.add(
				new SearchExpressionPair<T>(QueryOperation.OR, pSecondSearchExpression));
		return this;
	}

	/**
	 * returns the Source
	 * @return
	 */
	public Attribute<T> getSource() {
		return source;
	}

	/**
	 * returns the Target
	 * @return
	 */
	public Attribute<T> getTarget() {
		return target;
	}

	/**
	 * returns the Value
	 * @return
	 */
	public T getValue() {
		return value;
	}
	
	/**
	 * returns the Operator used for comparison
	 * @return
	 */
	public QueryOperation getExpressionOperator() {
		return operator;
	}

	public QueryOperation getParentOperator() {
		return parentOperator;
	}
	
	public SearchExpression<T> getParentSearchExpression() {
		return parentSearchExpression;
	}

	/**
	 * Get all SearchExpressions that are added to the given SearchExpression
	 * @return List of all SearchExpressions
	 */
	public List<SearchExpressionPair<T>> getChildSearchExpression() {
		return this.childSearchExpression;
	}
	
	/**
	 * Returns true if there are any SearchExpressions added to this SearchExpression instance
	 * @return true/false
	 */
	public boolean hasChildSearchExpression() {
		return this.childSearchExpression.size() > 0;
	}
}
