//Copyright (C) 2012  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.businessobject;

import org.nuclos.api.businessobject.attribute.Attribute;
import org.nuclos.api.businessobject.attribute.ForeignKeyAttribute;


/**
 * Query bases on a {@link BusinessObject}
 * 
 * @author Matthias Reichart
 */
public interface Query<T extends BusinessObject> {


	/**
	 * Use this method to set the Map of element that must be considered for ordering
	 * Attach for each field a Boolean.TRUE for ascending a boolean.FALSE for descending ordering
	 */
	Query<T> orderBy(Attribute element, Boolean ascending);
	Query<T> and (Attribute element, Boolean ascending);
	
	
	/**
	 * Add QueryExpressions for the search
	 */
	Query<T> where (SearchExpression elm);
	Query<T> and (SearchExpression elm);
	
	/**
	 * Combining the query with a subquery. The attribute must be a ForeignKey of the main query. It is used for field-comparison
	 * with the id of the subquery
	 * @param subQuery
	 * @param element
	 */
	<P extends BusinessObject> Query<T> exist(Query<P> subQuery, ForeignKeyAttribute element);
	
	
	/**
	 * Combining the query with a subquery. The given attributes are used for field-comparison.
	 * Notice that both fields must be of type ForeignKeyAttribute
	 * @param subQuery
	 * @param elementMainQuery
	 * @param elementSubQuery
	 * @return
	 */
	<P extends BusinessObject> Query<T> exist(Query<P> subQuery, ForeignKeyAttribute elementMainQuery,
			ForeignKeyAttribute elementSubQuery);
}
