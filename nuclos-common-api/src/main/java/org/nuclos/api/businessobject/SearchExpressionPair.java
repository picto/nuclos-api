package org.nuclos.api.businessobject;

/**
 * {@link SearchExpressionPair} represents a pair of search expression and operation qualifier
 * 
 * <p> 
 * @author Matthias Reichart
 */
public class SearchExpressionPair<T> {


	private QueryOperation operator;
	private SearchExpression<T> searchExp;
	
	public SearchExpressionPair(QueryOperation pOperator, SearchExpression<T> pSearchExp) {
		this.operator = pOperator;
		this.searchExp = pSearchExp;
	}

	public QueryOperation getOperator() {
		return operator;
	}

	public SearchExpression<T> getSearchExpression() {
		return searchExp;
	}
	
	
}
