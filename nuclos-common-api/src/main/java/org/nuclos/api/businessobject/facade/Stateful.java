//Copyright (C) 2012  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.businessobject.facade;

/**
 * May be deprecated in future release.
 * 
 * Use the new 'thin' interface to improve the performance of your business object!
 */
public interface Stateful extends org.nuclos.api.businessobject.facade.thin.Stateful {
	
	/**
	 * Returns the default name of the current state
	 * @return
	 */
	String getNuclosState();
	
	/**
	 * Returns the number of the current state
	 * @return
	 */
	Integer getNuclosStateNumber();
}
