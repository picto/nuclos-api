package org.nuclos.api.businessobject;

import org.nuclos.api.UID;
import org.nuclos.api.businessobject.facade.thin.Stateful;

/**
 * The process class represents a process in nuclos;<br>It is assigned to an entity that uses a statemodel and
 * is used in rule-programming. See QueryProvider.
 * @author reichama
 *
 * @param <T>
 */
public class Process<T extends Stateful> {

	private UID id;
	private UID entityId;
	private Class<T> type;
	
	/**
	 * Constructor using the Id of the process, the Id of the entity and class-type of the {@link BusinessObject}
	 * @param pId
	 * @param pEntityId
	 * @param pType
	 */
	public Process(UID pId, UID pEntityId, Class<T> pType) {
		this.id = pId;
		this.entityId = pEntityId;
		this.type = pType;
	}

	
	/**
	 * Returns the Id of the process
	 * @return
	 */
	public UID getId() {
		return id;
	}

	/**
	 * Returns the Id of the entity
	 * @return
	 */
	public UID getEntityId() {
		return entityId;
	}

	/**
	 * Returns the class-type of the {@link BusinessObject} the process belongs to
	 * @return
	 */
	public Class<T> getType() {
		return type;
	}
	
	
}
