//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.print;

import java.io.Serializable;

import org.nuclos.api.UID;
import org.nuclos.api.report.OutputFormat;


/**
 * class used in {@link OutputFormat} and {@link FileProvider} 
 * setup properties to control the printing process
 *  
 * @author Moritz Neuhaeuser <moritz.neuhaeuser@nuclos.de>
 *
 */
public class PrintProperties implements Serializable {
	
	private static final long serialVersionUID = -5423858750426473278L;
	
	private int copies = 1;
	private boolean duplex = false;
	private UID trayId;
	private UID printServiceId;

	/**
	 * get quantity of printed pages
	 * 
	 * @return quantity of printouts 
	 */
	public int getCopies() {
		return copies;
	}
	
	/**
	 * set quantity of printed pages
	 * 
	 * @param quantity quantity
	 */
	public void setCopies(int copies) {
		this.copies = copies;
	}
	
	/**
	 * get is duplex mode enabled
	 * 
	 * @return is duplex mode enabled
	 */
	public boolean isDuplex() {
		return duplex;
	}
	
	/**
	 * set is duplex mode enabled
	 * 
	 * @param value
	 */
	public void setDuplex(boolean duplex) {
		this.duplex = duplex;
	}
	
	/**
	 * get tray of the print service
	 * 
	 * @return tray id of the print service
	 */
	public UID getTrayId() {
		return trayId;
	}
	
	/**
	 * set tray {@link UID}  of the print service
	 * 
	 * @param tray	tray
	 */
	public void setTrayId(final UID trayId) {
		this.trayId = trayId;
	}
	
	/**
	 * get print service is used for printouts
	 * 
	 * @return print service id used for printouts
	 */
	public UID getPrintServiceId() {
		return printServiceId;
	}
	
	/**
	 * set print service {@link UID}  used for printouts
	 * 
	 * @param printService	print service 
	 */
	public void setPrintServiceId(final UID printServiceId) {
		this.printServiceId = printServiceId;
	}

}
