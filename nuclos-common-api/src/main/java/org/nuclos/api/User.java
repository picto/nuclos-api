package org.nuclos.api;

import java.util.Date;
import java.util.Set;

import org.nuclos.api.locale.NuclosLocale;

public interface User {

	public UID getId();
	public String getName();
	public String getLastname();
	public String getEmail();
	public String getFirstname();
	public Boolean isSuperuser();
	public Boolean isLocked();
	public Date getPasswordChanged();
	public Date getExpirationDate();
	public Boolean isRequirePasswordChange();
	public NuclosLocale getDataLocale();
}
