//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.report;

import org.nuclos.api.UID;
import org.nuclos.api.print.PrintProperties;
import org.nuclos.api.printout.Printout;


/**
 * Interface used in rule programming to determine the format of {@link Printout} and {@link Report} objects 
 * @author reichama
 *
 */
public interface OutputFormat {
	
	/**
	 * returns the id of the current format
	 * @return
	 */
	public UID getId();
	
	/**
	 * returns {@link PrintProperties} 
	 * 
	 * details of the print process
	 *  
	 * @return
	 */
	public PrintProperties getProperties();
	
}
