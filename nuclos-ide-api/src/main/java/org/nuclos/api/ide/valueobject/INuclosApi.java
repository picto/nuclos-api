package org.nuclos.api.ide.valueobject;

import java.util.Set;

public interface INuclosApi {
	
	/**
	 * Central value object interface for nuclos api information.
	 * 
	 * @author Maik Stueker
	 * @since Nuclos 3.13
	 */
	String getNuclosVersion();

	Set<String> getNuclosApiJars();
	
}
