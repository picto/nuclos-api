package org.nuclos.api.ide.valueobject;

import java.io.Serializable;
import java.util.Set;

/**
 * Central value object implementation for nuclos api information.
 * 
 * @author Maik Stueker
 * @since Nuclos 3.13
 */
public class NuclosApi implements INuclosApi, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private final String nuclosVersion;
	private final Set<String> nuclosApiJars;
	
	public NuclosApi(String nuclosVersion, Set<String> nuclosApiJars) {
		super();
		this.nuclosVersion = nuclosVersion;
		this.nuclosApiJars = nuclosApiJars;
	}

	@Override
	public String getNuclosVersion() {
		return nuclosVersion;
	}

	@Override
	public Set<String> getNuclosApiJars() {
		return nuclosApiJars;
	}

}
