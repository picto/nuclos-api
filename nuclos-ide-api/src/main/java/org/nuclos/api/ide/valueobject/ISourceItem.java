//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.ide.valueobject;

import java.io.Serializable;
import java.util.Collection;

/**
 * Central value object interface for supporting java event support sources synchronization to the client.
 * 
 * @author Thomas Pasch
 * @since Nuclos 3.13
 */
public interface ISourceItem {

	String getQualifiedName();

	void setQualifiedName(String qualifiedName);

	/**
	 * Returns a String representation of calculated crypto hash. No contract 
	 * about the representation (e.g. binary, hex, base64) is made. However, 
	 * the same object is guaranteed to use only <em>one</em> representation. Per
	 * consequence the representation could be used with 
	 * {@link Object#equals(Object)}.
	 */
	String getHashValue();

	void setHashValue(String hashValue);

	SourceType getType();

	void setType(SourceType type);
	
	/**
	 * Attention:
	 * As we cache the nodes, the children are normally NOT up-to-date.
	 * As we re-create the tree struction we must update (and re-connect)
	 * the nodes.
	 */
	Collection<? extends ISourceItem> getChildren();
	
	void addChild(ISourceItem child);
	
	void visit(ISourceVisitor visitor);
	
	/**
	 * Attention:
	 * As we don't want the UID class in the eclipse plugin, the String
	 * representation of UIDs is returned. (tp) 
	 */
	Serializable getId();
	
	/**
	 * Attention:
	 * As we don't want the UID class in the eclipse plugin, UIDs must be
	 * converted to String before calling this setter. (tp)
	 */
	void setId(Serializable id);
	
}
