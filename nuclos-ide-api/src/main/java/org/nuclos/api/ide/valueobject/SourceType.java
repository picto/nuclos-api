//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.ide.valueobject;

/**
 * Types of {@link ISourceItem}s that are synchronized to the client.
 * 
 * @author Thomas Pasch
 * @since Nuclos 3.13
 */
public enum SourceType {
	
	/*
	 * Attention: SEQUENCE IS IMPORTANT! We want PACKAGE first (for compareTo(..))! (tp)
	 */
	
	/**
	 * Nothing will be synchronized. Only a container hash for contained items.
	 */
	PACKAGE,
	
	/**
	 * The java source will be synchronized.
	 * 
	 * org.nuclos.server.customcode.codegenerator.NuclosCodegeneratorConstants.JAR_SRC_FOLDER
	 * org.nuclos.server.customcode.codegenerator.NuclosCodegeneratorConstants.JARFILE
	 */
	EVENT_SUPPORT_CLASS,
	
	/**
	 * The java class file will be synchronized.
	 * 
	 * org.nuclos.server.customcode.codegenerator.NuclosCodegeneratorConstants.BO_SRC_FOLDER
	 * org.nuclos.server.customcode.codegenerator.NuclosCodegeneratorConstants.BOJARFILE
	 */
	BO_ENTITIES,
	
	/**
	 * The java class file will be synchronized.
	 * 
	 * org.nuclos.server.customcode.codegenerator.NuclosCodegeneratorConstants.DATASOURCEREPORT_SRC_FOLDER
	 * org.nuclos.server.customcode.codegenerator.NuclosCodegeneratorConstants.DATASOURCEREPORTJARFILE
	 */
	DATASOURCE_REPORT,
	
	/**
	 * The java class file will be synchronized.
	 * 
	 * org.nuclos.server.customcode.codegenerator.NuclosCodegeneratorConstants.IMPORTSTRUCTUREDEFINITIONS_SRC_FOLDER
	 * org.nuclos.server.customcode.codegenerator.NuclosCodegeneratorConstants.IMPORTSTRUCTUREDEFINITIONSJARFILE
	 */
	IMPORTSTRUCTUREDEFINITIONS,
	
	/**
	 * The java class file will be synchronized.
	 * 
	 * org.nuclos.server.customcode.codegenerator.NuclosCodegeneratorConstants.PARAMETER_SRC_FOLDER
	 * org.nuclos.server.customcode.codegenerator.NuclosCodegeneratorConstants.PARAMETERJARFILE
	 */
	PARAMETER,
	
	/**
	 * The java class file will be synchronized.
	 * 
	 * org.nuclos.server.customcode.codegenerator.NuclosCodegeneratorConstants.COMMUNICATION_SRC_FOLDER
	 * org.nuclos.server.customcode.codegenerator.NuclosCodegeneratorConstants.COMMUNICATIONJARFILE
	 */
	COMMUNICATION,
	
	/**
	 * The java class file will be synchronized.
	 * 
	 * org.nuclos.server.customcode.codegenerator.NuclosCodegeneratorConstants.STATEMODEL_SRC_FOLDER
	 * org.nuclos.server.customcode.codegenerator.NuclosCodegeneratorConstants.STATEMODELJARFILE
	 */
	STATEMODEL,
	
	/**
	 * The java class file will be synchronized.
	 * 
	 * org.nuclos.server.customcode.codegenerator.NuclosCodegeneratorConstants.REPORT_SRC_FOLDER
	 * org.nuclos.server.customcode.codegenerator.NuclosCodegeneratorConstants.REPORTJARFILE
	 */
	REPORT,
	
	/**
	 * The java class file will be synchronized.
	 * 
	 * org.nuclos.server.customcode.codegenerator.NuclosCodegeneratorConstants.WEBSERVICE_SRC_FOLDER
	 * org.nuclos.server.customcode.codegenerator.NuclosCodegeneratorConstants.WEBSERVICEJARFILE
	 */
	WEBSERVICE,
	
	
	/**
	 * The java class file will be synchronized.
	 * 
	 * org.nuclos.server.customcode.codegenerator.NuclosCodegeneratorConstants.PRINTOUT_SRC_FOLDER
	 * org.nuclos.server.customcode.codegenerator.NuclosCodegeneratorConstants.PRINTOUTJARFILE
	 */
	PRINTOUT,
	
	/**
	 * The java class file will be synchronized.
	 * 
	 * org.nuclos.server.customcode.codegenerator.NuclosCodegeneratorConstants.USERGROUP_SRC_FOLDER
	 * org.nuclos.server.customcode.codegenerator.NuclosCodegeneratorConstants.USERGROUPJARFILE
	 */
	USERROLE,
	
	/**
	 * The java class file will be synchronized.
	 * 
	 * org.nuclos.server.customcode.codegenerator.NuclosCodegeneratorConstants.GENERATION_SRC_FOLDER
	 * org.nuclos.server.customcode.codegenerator.NuclosCodegeneratorConstants.GENERATIONJARFILE
	 */
	GENERATION,
	
	/**
	 * The java class file will be synchronized.
	 * 
	 * org.nuclos.server.customcode.codegenerator.NuclosCodegeneratorConstants.CCCEJARFILE
	 */
	CODE_COMPILER_CLASS_EXTENSION;
	
	public static boolean isNuclosBusinessObject(SourceType type) {
		if (type == null) throw new NullPointerException();
		final boolean result;
		switch (type) {
		case EVENT_SUPPORT_CLASS:
			result = false;
			break;
		case BO_ENTITIES:
		case DATASOURCE_REPORT:
		case CODE_COMPILER_CLASS_EXTENSION:
		case STATEMODEL:
		case WEBSERVICE:
		case GENERATION:
			result = true;
			break;
		case PACKAGE:
		default:
			throw new IllegalArgumentException(type.toString());
		}
		return result;
	}

}
