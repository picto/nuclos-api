//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.api.ide.valueobject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

/**
 * Central value object implementation for supporting java event support sources synchronization to the client.
 * 
 * @author Thomas Pasch
 * @since Nuclos 3.13
 */
public class SourceItem implements ISourceItem, Serializable {
	
	private String qualifiedName;
	
	private String hashValue;
	
	private SourceType type;
	
	private Collection<ISourceItem> children;
	
	private Serializable id;
	
	/**
	 * Attention:
	 * Constructor is normally NOT called. Use the factory method of SourceCache.
	 */
	public SourceItem() {
	}

	@Override
	public String getQualifiedName() {
		return qualifiedName;
	}

	@Override
	public void setQualifiedName(String qualifiedName) {
		this.qualifiedName = qualifiedName;
	}

	@Override
	public String getHashValue() {
		return hashValue;
	}

	@Override
	public void setHashValue(String hashValue) {
		this.hashValue = hashValue;
	}

	@Override
	public SourceType getType() {
		return type;
	}

	@Override
	public void setType(SourceType type) {
		this.type = type;
	}

	@Override
	public Collection<? extends ISourceItem> getChildren() {
		if (children == null) {
			return Collections.emptyList();
		} else {
			return children;
		}
	}

	@Override
	public void addChild(ISourceItem child) {
		if (children == null) {
			children = new ArrayList<ISourceItem>();
		}
		children.add(child);
	}

	@Override
	public void visit(ISourceVisitor visitor) {
		visitor.process(this);
	}
	
	@Override
	public Serializable getId() {
		return id;
	}

	@Override
	public void setId(Serializable id) {
		this.id = id;
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == this) return true;
		if (!(o instanceof ISourceItem)) return false;
		final ISourceItem other = (ISourceItem) o;
		boolean result = true;
		if (getQualifiedName() != null && other.getQualifiedName() != null) {
			result = result && getQualifiedName().equals(other.getQualifiedName());
		}
		if (getHashValue() != null && other.getHashValue() != null) {
			result = result && getHashValue().equals(other.getHashValue());
		}
		if (getId() != null && other.getId() != null) {
			result = result && getId().equals(other.getId());
		}
		if (getType() != null && other.getType() != null) {
			result = result && getType() == other.getType();
		}
		return result;
	}
	
	@Override
	public int hashCode() {
		int result = 7391;
		if (getQualifiedName() != null) {
			result += 3 * getQualifiedName().hashCode();
		}
		if (getHashValue() != null) {
			result += 7 * getHashValue().hashCode();
		}
		if (getId() != null) {
			result += 11 * getId().hashCode();
		}
		if (getType() != null) {
			result += 13 * getType().hashCode();
		}
		return result;
	}
	
	@Override
	public String toString() {
		final StringBuilder result = new StringBuilder();
		result.append("SourceItem[");
		result.append(qualifiedName);
		result.append(",");
		result.append(type);
		if (id != null) {
			result.append(", id=").append(id);
		}
		if (hashValue != null) {
			result.append(", hash=").append(hashValue);
		}
		result.append("]");
		return result.toString();		
	}

}
